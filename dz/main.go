package main

import "fmt"

func main() {
	matrix := [][]int{{1, 2, 4}, {8, 9, 1}, {10, 10, 10}} //(1)

	rows := len(matrix)    //(2)
	cols := len(matrix[0]) //(3)

	min_sum := 1<<63 - 1 //(4)
	sum := 0             //(5)

	for i := 0; i < rows; i++ { // 6
		sum = 0                     //(7)
		for j := 0; j < cols; j++ { //8
			sum += matrix[i][j] //(9)
		}
		if sum < min_sum { //(10)
			min_sum = sum // (11)
		}
	}

	max_sum := -1 << 63         //(12)
	for i := 0; i < rows; i++ { //(13)
		sum = 0                     //(14)
		for j := 0; j < cols; j++ { //(15)
			sum += matrix[i][j] //(16)
		}
		if sum > max_sum { //(17)
			max_sum = sum //(18)
		}
	}

	for i := 0; i < rows; i++ { //(19)
		for j := 0; j < cols; j++ { // (20)
			matrix[i][j] = max_sum + min_sum //(21)
		}
	}

	fmt.Printf("%v", matrix)
}