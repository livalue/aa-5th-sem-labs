package main

import (
	"fmt"
	"log"
	"os"
)

const (
	RIGHT = iota
	DOWN
	LEFT
	UP
	SLIDE
	SLIDEDOWN
)

func check(left, right, middle uint64)bool{
	if left > right{
		left, right = right, left
	}
	return left <= middle && middle <= right
}

func determine(row, col, nextRow, nextCol, y, x uint64)bool{
	//fmt.Println(row, col, nextRow, nextCol, y, x)
	if row == y{
		return check(col, nextCol, x)
	}else if col == x{
		return check(row, nextRow, y)
	}
	return false
}

func getTarget(row, col, x, y, total uint64, dir int)uint64{
	var colInc,rowInc, totInc uint64 = 0, 0, 0
	switch dir {
	case RIGHT:
		colInc = -1
		totInc = colInc
	case DOWN:
		rowInc = -1
		totInc = rowInc
	case LEFT:
		colInc = 1
		totInc = -1
	case UP:
		rowInc = 1
		totInc = -1
	case SLIDEDOWN:
		rowInc = -1
		totInc = rowInc
	case SLIDE:
		colInc = -1
		totInc = colInc
	}

	//fmt.Println(row, col, colInc, rowInc, x, y)
	for col != x{
		col += colInc
		total += totInc
	}

	for row != y{
		row += rowInc
		total += totInc
	}
	//fmt.Println("res = ", row, col, total)
	return total
}
func calc(y, x uint64) uint64{
	flag := false

	var row, col, total uint64 = 1, 1, 1
	var lc, rc uint64 = 1, 2
	var dc, uc uint64 = 1, 2

	currDir := SLIDE
	nextDir := currDir

	for !flag{
		//fmt.Println(row, col, x, y)
		currDir = nextDir
		if currDir == RIGHT{
			flag = determine(row, col, 0, col + rc, y, x)
			col += rc
			total += rc
			rc += 2
			nextDir = UP
		}else if currDir == DOWN{
			flag = determine(row, col, row + dc, 0, y, x)
			row += dc
			total += dc
			dc += 2
			nextDir = LEFT
		}else if currDir == LEFT{
			flag = determine(row, col, 0, col - lc, y, x)
			col -= lc
			total += lc
			lc += 2
			nextDir = SLIDEDOWN
		}else if currDir == UP{
			flag = determine(row, col, row - uc,0, y, x)
			row -= uc
			total += uc
			uc += 2
			nextDir = SLIDE
		}else if currDir == SLIDE{
			flag = determine(row, col, 0, col + 1, y, x)
			col++
			total++
			nextDir = DOWN
		}else if currDir == SLIDEDOWN{
			flag = determine(row, col, row + 1,0, y, x)
			row++
			total++
			nextDir = RIGHT
		}

	}

	//fmt.Println(row, col)

	//fmt.Println("total = ", total, "curr = ", currDir)

	return getTarget(row, col, x, y, total, currDir)
}

func main(){
	f, err := os.Open("input.txt")
	if err != nil{
		log.Fatal("there is no such file")
	}
	defer f.Close()

	o, err := os.Create("output.txt")
	if err != nil{
		log.Fatal("cant create such file")
	}
	defer o.Close()

	var count int
	var x, y uint64

	fmt.Fscanln(f, &count)

	fmt.Println("count = ", count)

	for i := 0; i < count; i++{
		fmt.Fscanln(f, &y, &x)
		fmt.Println("i = ", i)
		fmt.Fprintln(o, calc(y, x))
	}



}
