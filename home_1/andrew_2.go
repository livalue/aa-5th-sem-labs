package main

import (
	"fmt"
	"log"
	"os"
)

func calcL(y, x, start int64)int64{

	var row, col, colInc, rowInc int64

	for row != y || col != x {
		//fmt.Println("start = ", start)
		//fmt.Println(row, col, rowInc, colInc)
		if row == 1 && col % 2 == 0{
			start--
			col--
			rowInc = 1
			colInc = 0
			continue
		}else if col == 1 && row % 2 != 0{
			start--
			row--
			colInc = 1
			rowInc = 0
			continue
		}else if row == col {
			rowInc, colInc = colInc, rowInc
			rowInc *= -1
			colInc *= -1
		}

		//if start > 889344699930098742{
		//	fmt.Println(row, col, rowInc, colInc, start)
		//	break
		//}

		col += colInc
		row += rowInc
		start--
	}

	return start
}

func calcR(y, x, start int64)int64{
	if x == 1 && y == 1{
		return 1
	}
	var row, col int64 = 1, 1
	var colInc, rowInc int64 = 0, 0

	for row != y || col != x {
		fmt.Println("start = ", start)
		//fmt.Println(row, col, rowInc, colInc)
		if row == 1 && col % 2 != 0{
			start++
			col++
			rowInc = 1
			colInc = 0
			continue
		}else if col == 1 && row % 2 == 0{
			start++
			row++
			colInc = 1
			rowInc = 0
			continue
		}else if row == col {
			rowInc, colInc = colInc, rowInc
			rowInc *= -1
			colInc *= -1
		}

		if start > 889344699930098742{
			fmt.Println(row, col, rowInc, colInc, start)
			break
		}

		col += colInc
		row += rowInc
		start++

	}

	return start
}

func det(y, x int64)int64{
	var sq = x
	if y >= x{
		if y % 2 == 0{
			sq = y * y
			return calcL(y, x, sq * sq) 
		}else{
			sq = (y - 1) * (y -1)
			y -= 1
			return calcR(y, x, sq * sq)
		}
	}

	if x % 2 == 0{
		sq = (x - 1) * (x - 1)
		x -= 1
		return calcR(y, x, sq * sq)
	}else{
		sq = x * x
		return calcL(y, x, sq * sq)
	}
}

func main(){
	f, err := os.Open("input.txt")
	if err != nil{
		log.Fatal("there is no such file")
	}
	defer f.Close()

	o, err := os.Create("output.txt")
	if err != nil{
		log.Fatal("cant create such file")
	}
	defer o.Close()

	var count int
	var x, y int64

	fmt.Fscanln(f, &count)

	fmt.Println("count = ", count)

	for i := 0; i < count; i++{
		fmt.Fscanln(f, &y, &x)
		fmt.Println("i = ", i)
		fmt.Fprintln(o, det(y, x))
	}
}
