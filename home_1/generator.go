package main

import (
	"fmt"
	"log"
	"os"
)

func main(){
	f, err := os.Create("input.txt")
	if err != nil{
		log.Fatal("cant create input file")
	}

	defer f.Close()
	//s1 := rand.NewSource(time.Now().UnixNano())
	//r1 := rand.New(s1)

	//amount := r1.Intn( 1000) + 1
	amount := 7
	fmt.Fprintln(f, amount)
	for i := 0; i < amount; i++{
		for j := 0; j < amount; j++{
			fmt.Fprintln(f, i + 1, j + 1)
		}
		//fmt.Fprintln(f, r1.Intn(10000) + 1, r1.Intn(10000) + 1)
	}

}
