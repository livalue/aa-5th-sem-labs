package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func get(y, x int64)int64{
	var colInc, rowInc, sq, ans int64
	if y <= x{
		sq = x * x
		colInc = -1
		if x % 2 == 0{
			sq = (x - 1) * (x - 1) + 1
			colInc = 1
		}
		ans = sq + (y - 1) * colInc

	}else{
		sq = y * y
		rowInc = -1
		if y % 2 != 0{
			sq = (y - 1) * (y - 1) + 1
			rowInc = 1
		}
		ans = sq + (x - 1) * rowInc
	}

	return ans
}
func main(){
	f, err := os.Open("input.txt")
	if err != nil{
		log.Fatal("there is no such file")
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Scan()

	o, err := os.Create("output.txt")
	if err != nil{
		log.Fatal("cant create such file")
	}
	defer o.Close()

	writer := bufio.NewWriter(o)

	var i, amount, x, y int64
	fmt.Sscanf(scanner.Text(), "%d", &amount)

	for ; i < amount; i++{
		scanner.Scan()
		fmt.Sscanf(scanner.Text(), "%d%d", &y, &x)
		writer.WriteString(fmt.Sprintf("%d\n", get(y, x)))
		writer.Flush()
	}
}
