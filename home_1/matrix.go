package main

import (
	"fmt"
	"github.com/pkg/errors"
	"math/rand"
	"time"
)

type Matrix struct {
	matrix [][]int
	rows int
	cols int
}

func newMatrix(r, c int)Matrix{
	matrix := make([][]int, r)
	for index := range matrix{
		matrix[index] = make([]int, c)
	}
	return Matrix{
		matrix: matrix,
		rows:   r,
		cols:   c,
	}
}

func (m Matrix) transpose() Matrix{
	matrix := make([][]int, m.cols)
	for i := range matrix{
		matrix[i] = make([]int, m.rows)
		for j := range matrix[i]{
			matrix[i][j] = m.matrix[j][i]
		}
	}
	return Matrix{
		matrix: matrix,
		rows:   m.cols,
		cols:   m.rows,
	}
}

func Mult(m, factor Matrix)(Matrix, error){
	transFactor := factor.transpose()
	output := newMatrix(m.rows, factor.cols)
	for i := range output.matrix{
		//fmt.Println("new row")
		for j := range output.matrix[i]{
			//fmt.Println("new col")
			for l, row := range transFactor.matrix[j]{
				//fmt.Println(m.matrix[i][l], row, output.matrix[i][j])
				output.matrix[i][j] += m.matrix[i][l] * row
			}
		}
	}
	return output, nil
}

func (m Matrix) mult(factor Matrix, f func(Matrix, Matrix)(Matrix, error)) (Matrix, error){
	if m.rows != factor.cols{
		return Matrix{}, errors.New("количество строк первой матрицы не равно количеству столбцов второй")
	}

	return f(m, factor)
}

func (m Matrix) print(){
	fmt.Println("matrix: ")
	for _, row := range m.matrix{
		for _, col := range row{
			fmt.Print(col, " ")
		}
		fmt.Println()
	}
}

func (m *Matrix) fill(){
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)

	for i := range m.matrix{
		for j := range m.matrix[i]{
			m.matrix[i][j] = r1.Intn(10)
		}
	}
}

//func (m * Matrix) zero(){
//	for i := range m.matrix{
//		for j := range m.matrix[i]{
//			m.matrix[i][j] = 0
//		}
//	}
//}

func input()(r, c int){
	fmt.Print("Enter rows for matrix = ")
	fmt.Scan(&r)
	fmt.Println()
	fmt.Print("Enter cols for matrix = ")
	fmt.Scan(&c)
	fmt.Println()
	return
}

func measure(times, amount int) int64{
	if times == 0 {
		return 0
	}
	matrix1 := newMatrix(amount, amount)
	matrix1.fill()
	matrix2 := newMatrix(amount, amount)
	matrix2.fill()
	var total time.Duration
	for i := 0; i < times; i++{
		start := time.Now()
		_, _ = matrix1.mult(matrix2, Mult)
		total += time.Since(start)
	}
	return total.Milliseconds() / int64(times)
}

func test(start, end, step, times int){
	for i := start; i <= end; i += step{
		fmt.Println("time = ", measure(times, i), " milliseconds with rows and cols =", i)
		times--
	}
}

//func BenchmarkTest(b *testing.B){
//	for i:= 0; i < b.N; i++{
//		test(100, 1000, 100, 10)
//	}
//	fmt.Println("With odd")
//}

func main(){

	test(100, 1000, 100, 10)
	fmt.Println("With odd")
	test(101, 1001, 100, 10)
}
