package main

import "fmt"

func canSolve(matrix *[9][9]int, row, col, target int)bool{
	for _, num := range (*matrix)[row]{
		if num == target{
			return false
		}
	}

	for _, r := range *matrix{
		if r[col] == target{
			return false
		}
	}


	rs := 3 * (row / 3)
	cs := 3 * (col / 3)
	//fmt.Println(rs, re, cs, ce)


	for i:= rs; i <= rs + 2; i++{
		for j := cs; j <= cs + 2; j++{
			if (*matrix)[i][j] == target{
				return false
			}
		}
	}

	return true
}

func gracefulPrint(matrix [9][9]int){
	fmt.Println()
	for _, row := range matrix{
		fmt.Println(row)
	}
	fmt.Println()
}

func recursion(matrix *[9][9]int, row, col int)bool{
	if col == 9{
		if row == 8{
			return true
		}
		col = 0
		row++
	}

	if matrix[row][col] != 0{
		return recursion(matrix, row, col + 1)
	}

	fmt.Println(row, col)
	gracefulPrint(*matrix)

	for target := 1; target < 10; target++{
		if canSolve(matrix, row, col, target){
			(*matrix)[row][col] = target
			if recursion(matrix, row, col + 1){
				return true
			}else{
				(*matrix)[row][col] = 0
			}
		}
	}

	return false
}

func solver(matrix [9][9]int)[9][9]int{
	answer := recursion(&matrix,0, 0)
	fmt.Println(answer)
	return matrix
}

func equals(matr1, matr2 [9][9]int) bool{
	for i := range matr1{
		for j := range matr2{
			if matr1[i][j] != matr2[i][j]{
				return false
			}
		}
	}
	return true
}
func main(){
	matrix := [9][9]int{
		{0, 8, 7,   6, 5, 4,   3, 2, 1},
		{2, 0, 0,   0, 0, 0,   0, 0, 0},
		{3, 0, 0,   0, 0, 0,   0, 0, 0},

		{4, 0, 0,   0, 0, 0,   0, 0, 0},
		{5, 0, 0,   0, 0, 0,   0, 0, 0},
		{6, 0, 0,   0, 0, 0,   0, 0, 0},

		{7, 6, 0,   0, 0, 0,   0, 0, 0},
		{8, 0, 0,   0, 0, 0,   0, 0, 0},
		{9, 0, 0,   0, 0, 0,   0, 0, 0},
	}

	matrix = solver(matrix)


	gracefulPrint(matrix)


}
