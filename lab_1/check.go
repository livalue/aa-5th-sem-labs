package main

import (
	"fmt"
	"runtime"
)

func measureMem(){
	var m1, m2 runtime.MemStats
	runtime.ReadMemStats(&m1)
	//var arr []int
	//arr = append(arr, 1)
	arr := make([][]int, 2)
	runtime.ReadMemStats(&m2)
	fmt.Println(m2.Alloc - m1.Alloc, m2.TotalAlloc - m1.TotalAlloc, m2.HeapAlloc - m1.HeapAlloc)
	fmt.Println(arr)
}