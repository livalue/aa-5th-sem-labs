import json

# замени на свой файл с данными
file = "some.txt"

# файл, куда запишется json
output = "json.txt"

json_arr = []
with open(file, "r", encoding="utf-8") as f :
    flag = False
    res = []
    for line in f.readlines() :
        line = line.strip()
        data = line.split(" ")
        if len(data) == 1 :
            json_arr.append({"name" : data[0], "res" : res})
            res.clear()
        elif len(data) == 2:
            res.append({"amount" : int(data[0]), "time" : int(data[1])})

with open(output, "w", encoding="utf-8") as f :
    json.dump(json_arr, f)
