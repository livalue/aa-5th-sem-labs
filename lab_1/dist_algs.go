package main

import "fmt"

func printMatrix(s1, s2 []rune, matrix [][]int){
	if !debug{
		return
	}

	fmt.Printf("Matrix:\n")
	for i, ch := range s2{
		if i == 0{
			fmt.Printf("%4s", " ")
			fmt.Printf("%4d", 0)
			fmt.Print(" ")
		}
		fmt.Printf("%4s", string(ch))
		fmt.Print(" ")
	}
	fmt.Println()
	for i := range matrix{
		if i == 0{
			fmt.Printf("%4d", 0)
		}
		if i != 0{
			fmt.Printf("%4s", string(s1[i - 1]))
		}
		for j := range matrix[i]{
			fmt.Printf("%4d", matrix[i][j])
			if j != len(matrix[i]) - 1{
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}

}

func newMatrix(len1, len2 int, args ...int) [][]int{
	matrix := make([][]int, len1)
	for i := range matrix{
		matrix[i] = make([]int, len2)
		if len(args) != 0{
			for j := range matrix[i]{
				matrix[i][j] = args[0]
			}
		}
	}
	return matrix
}

func min(args... int) int{
	if len(args) == 0{
		return 0
	}

	var answer int = args[0]
	for _, elem := range args[1:]{
		if elem < answer{
			answer = elem
		}
	}

	return answer
}

func check(s1, s2 rune)int{
	if s1 == s2{
		return 0
	}
	return 1
}

func levin(s1, s2 []rune)int{
	matrix := newMatrix(len(s1) + 1, len(s2) + 1)
	for i := range matrix{
		for j := range matrix[i]{
			if i == 0{
				matrix[i][j] = j
			}else if j == 0{
				matrix[i][j] = i
			}else{
				matrix[i][j] = min(matrix[i][j-1] + 1, matrix[i - 1][j] + 1,
					               matrix[i-1][j-1] + check(s1[i - 1], s2[j - 1]))
			}
		}
	}
	printMatrix(s1, s2, matrix)
	return matrix[len(s1)][len(s2)]
}

func levinTwo(s1, s2 []rune)int{
	var dist func(int, int)int
	dist = func (i, j int)int{
		if i == 0{
			return j
		}else if j == 0{
			return i
		}
		return min(dist(i, j - 1) + 1, dist(i - 1, j) + 1, dist(i - 1, j - 1) + check(s1[i - 1], s2[j - 1]))
	}
	return dist(len(s1), len(s2))
}

func levinThird(s1, s2 []rune) int{
	matrix := newMatrix(len(s1) + 1, len(s2) + 1, -1)
	var dist func(int, int)int
	dist = func (i, j int)int{
		if i == 0{
			matrix[i][j] = j
			return j
		}else if j == 0{
			matrix[i][j] = i
			return i
		}else if matrix[i][j] != -1{
			return matrix[i][j]
		}
		matrix[i][j] = min(dist(i, j - 1) + 1, dist(i - 1, j) + 1, dist(i - 1, j - 1) + check(s1[i - 1], s2[j - 1]))
		return matrix[i][j]
	}
	defer printMatrix(s1, s2, matrix)
	return dist(len(s1), len(s2))
}

func damerau(s1, s2 []rune) int {
	matrix := newMatrix(len(s1) + 1, len(s2) + 1)
	for i := range matrix{
		for j := range matrix[i]{
			if i == 0{
				matrix[i][j] = j
			}else if j == 0{
				matrix[i][j] = i
			}else{
				matrix[i][j] = min(matrix[i][j-1] + 1, matrix[i - 1][j] + 1,
					               matrix[i-1][j-1] + check(s1[i - 1], s2[j - 1]))
				if i > 1 && j > 1 && s1[i - 1] == s2[j - 2] && s1[i - 2] == s2[j - 1] {
					matrix[i][j] = min(matrix[i][j], matrix[i-2][j-2]+1)
				}
			}
		}
	}
	printMatrix(s1, s2, matrix)
	return matrix[len(s1)][len(s2)]
}