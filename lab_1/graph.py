import json
import matplotlib.pyplot as plt
from random import randint

# Здесь напиши файл, из которого хочешь данные считать
file = "only_rec.txt"
# Название картинки
img = "only_rec.png"

def genArrs(obj) :
    x = []
    y = []
    for elem in obj["res"] :
        x.append(elem["amount"])
        y.append(elem["time"])
    return x, y


with open(file, "r") as f :
    data = json.load(f)

# ну тут понятно цвет линий, можешь дальше явно задать либо вкинуть еще цветов в массив
colors = ['blue', 'red', 'green']
# если запустишь со своим файлом увидишь метки на линиях, треугольники, кружки, как
# раз за это маркеры и отвечают
markers = ['^', 'o']

for obj in data:
    name = obj["name"]
    x, y = genArrs(obj)
    color = colors[randint(0, len(colors) - 1)]
    marker = markers[randint(0, len(markers) - 1)]
    plt.plot(x, y, color, marker=marker, markersize=15, markerfacecolor="white", label=name)

plt.grid(True)

plt.xlabel("Длина строк")
plt.ylabel("Наносекунды")

plt.legend()

# Для сохранения картинки, поменяй на что хочешь
plt.savefig(img)

plt.show()



