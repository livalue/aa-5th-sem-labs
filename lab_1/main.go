package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"time"
	"math/rand"
)

const (
	charset = "qwertyuiopasdfghjklzxcvbnmйцукенгшщзхфывапролджячсмитьбю"
	lev = "Levenshtein with matrix"
	levRecM = "Levenshtein with recursion and memoization"
	damer = "Damerau Levenshtein matrix"
	levRec = "Levenshtein with recursion"
	eps = 1e-8
)

var r = rand.New(rand.NewSource(time.Now().UnixNano()))

var debug = true

type alg func([]rune, []rune)int

func checkWork(){
	var s, t []rune
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Enter first string - ")
	if scanner.Scan(){
		s = []rune(scanner.Text())
	}else{
		log.Fatal("invalid string")
	}
	fmt.Print("Enter second string - ")
	if scanner.Scan(){
		t = []rune(scanner.Text())
	}else{
		log.Fatal("invalid string")
	}

	var ans, dist int
	flag := true

	for flag{
		fmt.Print(
			"Choose algorithm:\n",
			"1 - Levenshtein Matrix\n",
			"2 - Levenshtein Recursion(The worthiest algorithm)\n",
			"3 - Levenshtein Recursion with memoization\n",
			"4 - Damerau Levenshtein\n",
			"Your choice - ",
		)

		if amount, _ := fmt.Scanln(&ans); amount == 0{
			fmt.Println("Wrong input data try again")
			continue
		}

		switch ans {
		case 1:
			dist = levin(s, t)
		case 2:
			dist = levinTwo(s, t)
		case 3:
			dist = levinThird(s, t)
		case 4:
			dist = damerau(s, t)
		default:
			fmt.Println("Wrong choice number")
			continue
		}
		flag = false

	}

	fmt.Println("Minimum edit distance = ", dist)

}

func randString(length int) []rune {
	temp := []rune(charset)
	arr := make([]rune, length)
	for i := 0; i < length; i++ {
		arr[i] = temp[r.Intn(len(temp))]
	}
	return arr
}

func ranking(res map[string]int64){
	arr := make([]struct{
		name string
		t int64
	}, 0, len(res) - 1)

	for key, t := range res{
		if key == damer{
			continue
		}
		arr = append(arr, struct{
			name string
			t int64
		}{name: key, t: t})
	}

	sort.Slice(arr, func(i, j int)bool{
		return arr[i].t < arr[j].t
	})

	var tempFloat float64
	var output string

	for i := 0; i < len(arr) - 1; i++{
		fmt.Printf("%d)\n", i + 1)
		tempFloat = float64(arr[i].t)
		output = fmt.Sprint(arr[i].name, " faster than ")
		for j := i + 1; j < len(arr); j++{
			if tempFloat < eps{
				fmt.Println(output, arr[j].name)
			}else{
				fmt.Println(output, arr[j].name, "in", float64(arr[j].t) / tempFloat)
			}
		}
	}

}

func measure(){
	debug = false
	defer func() {debug = true}()
	var len1, len2 int
	fmt.Print("Enter length of strings separated by a space - ")
	if amount, _ := fmt.Scanln(&len1, &len2); amount != 2{
		fmt.Println("Wrong input")
		return
	}

	s := randString(len1)
	t := randString(len2)


	funcs := map[string]alg{
		lev: levin,
		levRec: levinTwo,
		levRecM: levinThird,
		damer: damerau,
	}

	times := make(map[string]int64)

	fmt.Println("\nSpeed measures:")

	var repeat = 700
	//println("s, t = ", string(s), string(t))
	for key, val := range funcs{
		var total int64
		var temp = repeat
		if key == levRec{
			temp = 80
		}

		for i := 0; i < temp; i++{
			start := time.Now()
			val(s, t)
			total += time.Since(start).Nanoseconds()
		}
		fmt.Println(key, "completed in", total / int64(repeat),"nanoseconds")
		times[key] = total
	}

	fmt.Println()

	if times[lev] > times[damer] {
		if times[damer] == 0{
			fmt.Println(damer, "faster than", lev)
		}else{
			fmt.Println(damer, "faster than", lev, "in ", float64(times[lev]) / float64(times[damer]), "times")
		}
	}else{
		if times[lev] == 0{
			fmt.Println(lev, "faster than", damer)
		}else{
			fmt.Println(lev, "faster than", damer, "in ", float64(times[damer]) / float64(times[lev]), "times")
		}
	}

	fmt.Println("\nComparison between Levenshtein's algorithms:")

	ranking(times)
}

func main(){
	var ans int
	flag := true
	for flag{
		fmt.Print(
			"########################MENU########################\n\n",
			"1 - Check work\n",
			"2 - Measure time\n",
			//"3 - Meausure for graph\n",
			"0 - exit\n\n",
			"Your choice - ",
			)
		if amount, _ := fmt.Scanln(&ans); amount == 0{
			fmt.Println("Wrong decimal number")
			continue
		}



		switch ans {
		case 1:
			checkWork()
		case 2:
			measure()
		//case 3:
		//	measureAll()
		case 0:
			flag = false
		default:
			fmt.Println("Wrong number of choice")
		}
	}
}
