package main

import "testing"

type Data struct {
	res []int
	data [2]string
}

var testStrings = []Data{
	{res: []int{0}, data:[2]string{"", ""}},
	{[]int{2},[2]string{"kot", "skat"}},
	{[]int{2, 1}, [2]string{"kate", "ktae"}},
	{[]int{4, 2}, [2]string{"abacaba", "aabcaab"}},
	{[]int{3}, [2]string{"sobaka", "sboku"}},
	{[]int{4}, [2]string{"qwerty", "queue"}},
	{[]int{2, 1}, [2]string{"apple", "aplpe"}},
	{[]int{3}, [2]string{"", "cat"}},
	{[]int{9}, [2]string{"parallels", ""}},
	{[]int{4}, [2]string{"bmstu", "utsmb"}},
}

func TestLevin(t *testing.T){
	debug = false
	for _, test := range testStrings{
		if !(test.res[0] == levin([]rune(test.data[0]), []rune(test.data[1]))){
			t.Errorf("incorrect answer with strings: %s and %s",
				test.data[0], test.data[1])
		}
	}
}

func TestLevinRec(t *testing.T){
	debug = false
	for _, test := range testStrings{
		if !(test.res[0] == levinTwo([]rune(test.data[0]), []rune(test.data[1]))){
			t.Errorf("incorrect answer with strings: %s and %s",
				test.data[0], test.data[1])
		}
	}
}

func TestLevinMem(t *testing.T){
	debug = false
	for _, test := range testStrings{
		if !(test.res[0] == levinThird([]rune(test.data[0]), []rune(test.data[1]))){
			t.Errorf("incorrect answer with strings: %s and %s",
				test.data[0], test.data[1])
		}
	}
}

func TestDamerau(t *testing.T){
	debug = false
	for _, test := range testStrings{
		res := test.res[0]
		if len(test.res) > 1{
			res = test.res[1]
		}
		if !(res == damerau([]rune(test.data[0]), []rune(test.data[1]))){
			t.Errorf("incorrect answer with strings: %s and %s",
				test.data[0], test.data[1])
		}
	}
}
