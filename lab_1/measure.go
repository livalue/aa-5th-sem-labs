package main
//#include <time.h>
import (
	"C"
	"encoding/json"
	"fmt"
	"os"
	"time"
)
type Data struct {
	Symbols int `json:"amount"`
	Time int64	`json:"time"`
}

type Measure struct {
	Name string `json:"name"`
	Res []Data `json:"res"`
}

func levinM(s1, s2 []rune)int{
	matrix := newMatrix(len(s1) + 1, len(s2) + 1)
	for i := range matrix{
		for j := range matrix[i]{
			if i == 0{
				matrix[i][j] = j
			}else if j == 0{
				matrix[i][j] = i
			}else{
				matrix[i][j] = min(matrix[i][j-1] + 1, matrix[i - 1][j] + 1,
								   matrix[i-1][j-1] + check(s1[i - 1], s2[j - 1]))
			}
		}
	}
	return matrix[len(s1)][len(s2)]
}

func levinRecM(s1, s2 []rune)int{
	var dist func(int, int)int
	dist = func (i, j int)int{
		if i == 0{
			return j
		}else if j == 0{
			return i
		}
		return min(dist(i, j - 1) + 1, dist(i - 1, j) + 1, dist(i - 1, j - 1) + check(s1[i - 1], s2[j - 1]))
	}
	return dist(len(s1), len(s2))
}

func levinMemM(s1, s2 []rune) int{
	matrix := newMatrix(len(s1) + 1, len(s2) + 1, -1)
	var dist func(int, int)int
	dist = func (i, j int)int{
		if i == 0{
			matrix[i][j] = j
			return j
		}else if j == 0{
			matrix[i][j] = i
			return i
		}else if matrix[i][j] != -1{
			return matrix[i][j]
		}
		matrix[i][j] = min(dist(i, j - 1) + 1, dist(i - 1, j) + 1, dist(i - 1, j - 1) + check(s1[i - 1], s2[j - 1]))
		return matrix[i][j]
	}
	return dist(len(s1), len(s2))
}

func damerauM(s1, s2 []rune) int {
	matrix := newMatrix(len(s1) + 1, len(s2) + 1)
	for i := range matrix{
		for j := range matrix[i]{
			if i == 0{
				matrix[i][j] = j
			}else if j == 0{
				matrix[i][j] = i
			}else{
				matrix[i][j] = min(matrix[i][j-1] + 1, matrix[i - 1][j] + 1,
					               matrix[i-1][j-1] + check(s1[i - 1], s2[j - 1]))

				if i > 1 && j > 1 && s1[i - 1] == s2[j - 2] && s1[i - 2] == s2[j - 1] {
					matrix[i][j] = min(matrix[i][j], matrix[i-2][j-2]+1)
				}
			}
		}
	}
	return matrix[len(s1)][len(s2)]
}

func writeFile(data []Measure, file string){
	js, err := json.Marshal(data)
	if err != nil{
		fmt.Printf("error occured while parsing data from meausre func" +
			" with err = %s", err)
	}

	f, err := os.Create(file)
	if err != nil{
		fmt.Printf("cant create file with name: %s due to: %s", file, err)
	}

	f.Write(js)
}

func measureForGraph(algs []string, start, stop, step, repeat int, file string){
	var data []Measure

	funcs := map[string]alg{
		lev: levinM,
		levRec: levinRecM,
		levRecM: levinMemM,
		damer: damerauM,
	}

	for _, name := range algs{
		fn := funcs[name]
		var res []Data
		for length := start; length <= stop; length += step{
			s := randString(length)
			t := randString(length)
			var total int64
			for i := 0; i < repeat; i++{
				start := time.Now()
				fn(s, t)
				total += time.Since(start).Nanoseconds()
			}
			res = append(res, Data{
				Symbols: length,
				Time:    total / int64(repeat),
			})
		}

		data = append(data, Measure{
			Name: name,
			Res:  res,
		})

	}

	writeFile(data, file)
}

func measureAll(){
	//first := []string{lev, damer}
	second := []string{levRecM}
	rec := []string{levRec}
	third := []string{lev, levRecM}

	//measureForGraph(first, 1000, 2000, 100, 10,"matrix.txt")
	measureForGraph(second, 1, 9, 1, 1500,"recM.txt")
	measureForGraph(rec, 1, 9, 1, 1500, "only_rec.txt")
	measureForGraph(third, 1, 9, 1, 1500,"levs.txt")
}
