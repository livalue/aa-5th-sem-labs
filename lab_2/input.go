package main

import (
	"errors"
	"fmt"
)

func input(rows, cols *int)error{
	fmt.Print("Input rows - ")
	if amount, err := fmt.Scanln(rows); amount == 0 || err != nil{
		return errors.New("wrong input")
	}
	fmt.Println()

	fmt.Print("Input cols - ")
	if amount, err := fmt.Scanln(cols); amount == 0 || err != nil{
		return errors.New("wrong input")
	}
	fmt.Println()
	return nil
}

func manulInput(matrix *Matrix) error{
	for i := 0; i < matrix.rows; i++{
		fmt.Printf("Input values for %d string:\n", i + 1)
		for j := 0; j < matrix.cols; j++{
			fmt.Printf("Enter value for [%d][%d] cell ->> ", i + 1, j + 1)
			if amount, err := fmt.Scanln(&matrix.matrix[i][j]); amount == 0 || err != nil{
				return errors.New("wrong input")
			}
		}
	}
	return nil
}

func menuInput(funcs map[string]method){
	var rows, cols, rows2, cols2 int
	if err := input(&rows, &cols); err != nil{
		fmt.Println(err)
		return
	}

	if err := input(&rows2, &cols2); err != nil{
		fmt.Println(err)
		return
	}

	if cols != rows2{
		fmt.Println("Incorrect size of matrices")
		return
	}

	matrix1 := newMatrix(rows, cols)
	matrix2 := newMatrix(rows2, cols2)

	fmt.Print(
		`Select the filling method:
	1 - Manual input
	2 - Random fill
	Your choice ->> `,
		)
	choice := 1
	if amount, err := fmt.Scanln(&choice); amount == 0 || err != nil{
		fmt.Println("Wrong input")
		return
	}

	switch choice {
	case 1:
		if err := manulInput(&matrix1); err != nil{
			fmt.Print(err)
			return
		}

		if err := manulInput(&matrix2); err != nil{
			fmt.Print(err)
			return
		}
	case 2:
		matrix1.fill()
		fmt.Println()
		matrix2.fill()
	}

	fmt.Println("Matrix first:")
	matrix1.print()
	fmt.Println()
	fmt.Println("Matrix second:")
	matrix1.print()
	fmt.Println()


	for met, f := range funcs{
		fmt.Println("Result of matrix multiplictation via ", met, "method:")
		f(matrix1, matrix2).print()
		fmt.Println()
	}

}
