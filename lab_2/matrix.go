package main

import (
	"fmt"
	"github.com/pkg/errors"
	"math/rand"
	"strconv"
	"sync"
	"time"
)

type Matrix struct {
	matrix [][]int
	rows int
	cols int
}

type method func(Matrix, Matrix)Matrix

const(
	simple = "Simple multiplication"
	vinogradov = "Simple vinogradov"
	improvedVinogradov = "Improved vinogradov"
)


func newMatrix(r, c int)Matrix{
	matrix := make([][]int, r)
	for index := range matrix{
		matrix[index] = make([]int, c)
	}
	return Matrix{
		matrix: matrix,
		rows:   r,
		cols:   c,
	}
}

func (m Matrix) transpose() Matrix{
	matrix := make([][]int, m.cols)
	for i := range matrix{
		matrix[i] = make([]int, m.rows)
		for j := range matrix[i]{
			matrix[i][j] = m.matrix[j][i]
		}
	}
	return Matrix{
		matrix: matrix,
		rows:   m.cols,
		cols:   m.rows,
	}
}


func WinCon(m, factor Matrix)Matrix{

	output := newMatrix(m.rows, factor.cols)
	mulH := make([]int, m.rows)
	mulV := make([]int, factor.cols)

	colMax := m.cols - 1
	odd := m.cols % 2

	wg := &sync.WaitGroup{}
	wg.Add(2)
	go func() {
		for i := 0; i < m.rows; i++{
			for j := 0; j < m.cols - 1; j += 2{
				mulH[i] += m.matrix[i][j] * m.matrix[i][j + 1]
			}
		}
		wg.Done()
	}()

	go func() {
		for i := 0; i < factor.cols; i++{
			for j := 0; j < m.cols - 1; j += 2{
				mulV[i] += factor.matrix[j][i] * factor.matrix[j + 1][i]
			}
		}
		wg.Done()
	}()

	wg.Wait()

	for i := 0; i < m.rows; i++{
		for j := 0; j < factor.cols; j++{
			wg.Add(1)
			go func(i, j int) {
				output.matrix[i][j] -= mulH[i] + mulV[j]
				if odd == 1{
					output.matrix[i][j] += m.matrix[i][colMax] * factor.matrix[colMax][j]
				}
				for k := 0; k < m.cols - 1; k += 2{
					output.matrix[i][j] += (m.matrix[i][k] + factor.matrix[k + 1][j]) *
						(m.matrix[i][k + 1] + factor.matrix[k][j])
				}
				wg.Done()
			}(i, j)
		}
	}

	wg.Wait()


	return output

}

func Con(m, factor Matrix)Matrix{
	output := newMatrix(m.rows, factor.cols)
	mulH := make([]int, m.rows)
	mulV := make([]int, factor.cols)

	colMax := m.cols - 1
	odd := m.cols % 2

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		for i := 0; i < m.rows; i++{
			wg.Add(1)
			go func(i int) {
				temp := mulH[i]
				for j := 0; j < m.cols - 1; j += 2{
					temp += m.matrix[i][j] * m.matrix[i][j + 1]
				}
				mulH[i] = temp
				wg.Done()
			}(i)
		}
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		for i := 0; i < factor.cols; i++{
			wg.Add(1)
			go func(i int) {
				temp := mulV[i]
				for j := 0; j < m.cols - 1; j += 2{
					temp += factor.matrix[j][i] * factor.matrix[j + 1][i]
				}
				mulV[i] = temp
				wg.Done()
			}(i)
		}
		wg.Done()
	}()

	wg.Wait()

	for i := 0; i < m.rows; i++{
		wg.Add(1)
		go func(i int) {
			for j := 0; j < factor.cols; j++ {
				temp := output.matrix[i][j]
				temp -= mulH[i] + mulV[j]
				if odd == 1 {
					temp += m.matrix[i][colMax] * factor.matrix[colMax][j]
				}
				for k := 0; k < m.cols-1; k += 2 {
					temp += (m.matrix[i][k] + factor.matrix[k+1][j]) *
						(m.matrix[i][k+1] + factor.matrix[k][j])
				}
				output.matrix[i][j] = temp
			}
			wg.Done()
		}(i)
	}

	wg.Wait()


	return output
}

func ModWinograd(m, factor Matrix)Matrix{
	output := newMatrix(m.rows, factor.cols)
	mulH := make([]int, m.rows)
	mulV := make([]int, factor.cols)

	colMax := m.cols - 1
	odd := m.cols % 2

	for i := 0; i < m.rows; i++{
		for j := 0; j < m.cols - 1; j += 2{
			mulH[i] += m.matrix[i][j] * m.matrix[i][j + 1]
		}
	}

	for i := 0; i < factor.cols; i++{
		for j := 0; j < m.cols - 1; j += 2{
			mulV[i] += factor.matrix[j][i] * factor.matrix[j + 1][i]
		}
	}

	var buff int
	for i := 0; i < m.rows; i++{
		for j := 0; j < factor.cols; j++{
			buff = -(mulH[i] + mulV[j])
			for k := 0; k < m.cols - 1; k += 2{
				buff += (m.matrix[i][k] + factor.matrix[k + 1][j]) *
					(m.matrix[i][k + 1] + factor.matrix[k][j])
			}
			if odd == 1{
				buff += m.matrix[i][colMax] * factor.matrix[colMax][j]
			}
			output.matrix[i][j] = buff
		}
	}

	return output
}

func Winograd(m, factor Matrix)Matrix{
	output := newMatrix(m.rows, factor.cols)
	mulH := make([]int, m.rows)
	mulV := make([]int, factor.cols)

	for i := 0; i < m.rows; i++{
		for j := 0; j < m.cols / 2; j++{
			mulH[i] = mulH[i] + m.matrix[i][2*j] * m.matrix[i][2*j + 1]
		}
	}

	for i := 0; i < factor.cols; i++{
		for j := 0; j < m.cols / 2; j++{
			mulV[i] = mulV[i] + factor.matrix[2*j][i] * factor.matrix[2*j + 1][i]
		}
	}

	for i := 0; i < m.rows; i++{
		for j := 0; j < factor.cols; j++{
			output.matrix[i][j] = -mulH[i] - mulV[j]
			for k := 0; k < m.cols / 2; k++{
				output.matrix[i][j] = output.matrix[i][j] + (m.matrix[i][2*k] + factor.matrix[2*k + 1][j]) *
					(m.matrix[i][2*k + 1] + factor.matrix[2*k][j])
			}
		}
	}

	if m.cols % 2 == 1{
		for i := 0; i < m.rows; i++{
			for j := 0; j < factor.cols; j++{
				output.matrix[i][j] = output.matrix[i][j] + m.matrix[i][m.cols - 1] * factor.matrix[m.cols - 1][j]
			}
		}
	}

	return output
}

func Standart(m, factor Matrix)Matrix{
	output := newMatrix(m.rows, factor.cols)
	for i := 0; i < m.rows; i++{
		for j := 0; j < factor.cols; j++{
			for k := 0; k < m.cols; k++{
				output.matrix[i][j] = output.matrix[i][j] + m.matrix[i][k] * factor.matrix[k][j]
			}
		}
	}
	return output
}

func (m Matrix) mult(factor Matrix, f method) (Matrix, error){
	if m.cols != factor.rows{
		return Matrix{}, errors.New("некорректные размеры матриц")
	}

	return f(m, factor), nil
}


func (m *Matrix) fill(){
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)

	for i := range m.matrix{
		for j := range m.matrix[i]{
			m.matrix[i][j] = r1.Intn(10)
		}
	}
}

func (m Matrix) findMaxLen() (max int){
	for _, row := range m.matrix{
		for _, col := range row{
			temp := len(strconv.Itoa(col))
			if temp > max{
				max = temp
			}
		}
	}
	return
}

func (m Matrix) print(){
	format := fmt.Sprint("%", m.findMaxLen(), "d ")
	for i := 0; i < m.rows; i ++{
		for j := 0; j < m.cols; j++{
			fmt.Printf(format, m.matrix[i][j])
		}
		fmt.Println()
	}

}

func main(){
	var ans int
	flag := true

	funcs := map[string]method{
		simple: Standart,
		vinogradov: Winograd,
		improvedVinogradov: ModWinograd,
	}

	for flag {
		fmt.Print(
			"########################MENU########################\n\n",
			"1 - Check work\n",
			"2 - Measure time\n",
			"0 - exit\n\n",
			"Your choice - ",
		)
		if amount, err := fmt.Scanln(&ans); amount == 0 || err != nil {
			fmt.Println("Wrong decimal number")
			continue
		}

		switch ans {
		case 1:
			menuInput(funcs)
		case 2:
			measureAll(funcs)
		//case 3:
		//	createFiles(funcs)
		case 0:
			flag = false
		default:
			fmt.Println("Wrong number of choice")
		}
	}

	fmt.Println("End of program")
}
