package main

import (
	"fmt"
	"testing"
)

func comp(m1, m2 Matrix) bool{
	for i := range m1.matrix{
		for j := range m1.matrix[i]{
			if m1.matrix[i][j] != m2.matrix[i][j]{
				return false
			}
		}
	}

	return true
}

func TestSizeOne(t *testing.T){
	m1 := newMatrix(1, 1)
	m2 := newMatrix(1, 1)


	m1.fill()
	m2.fill()

	res1, _ := m1.mult(m2, Standart)
	res2, _ := m1.mult(m2, Winograd)
	res3, _ := m1.mult(m2, ModWinograd)

	fmt.Println("Стандартный алгоритм умножения матриц")
	res1.print()
	fmt.Println("Алгоритм умножения Винограда")
	res2.print()
	fmt.Println("Оптимизированный алгоритм умножения Винограда")
	res3.print()

	if !(comp(res1, res2) && comp(res1, res3)){
		t.Error("matrix arent right")
	}
}

func TestSizeTwo(t *testing.T){
	m1 := newMatrix(2, 2)
	m2 := newMatrix(2, 2)

	m1.fill()
	m2.fill()

	res1, _ := m1.mult(m2, Standart)
	res2, _ := m1.mult(m2, Winograd)
	res3, _ := m1.mult(m2, ModWinograd)

	fmt.Println("Стандартный алгоритм умножения матриц")
	res1.print()
	fmt.Println("Алгоритм умножения Винограда")
	res2.print()
	fmt.Println("Оптимизированный алгоритм умножения Винограда")
	res3.print()

	if !(comp(res1, res2) && comp(res1, res3)){
		t.Error("matrix arent right")
	}
}

func TestErrorMatrix(t *testing.T){
	m1 := newMatrix(2, 5)
	m2 := newMatrix(4, 2)

	m1.fill()
	m2.fill()

	_, err1 := m1.mult(m2, Standart)
	_, err2 := m1.mult(m2, Winograd)
	_, err3 := m1.mult(m2, ModWinograd)

	fmt.Println("Стандартный алгоритм умножения матриц")
	fmt.Println(err1)
	fmt.Println("Алгоритм умножения Винограда")
	fmt.Println(err2)
	fmt.Println("Оптимизированный алгоритм умножения Винограда")
	fmt.Println(err3)

	if !(err1.Error() == err2.Error() && err2.Error() == err3.Error()){
		t.Error("Test corrupted because of invalid error")
	}
}

func TestEvenRows(t *testing.T){
	m1 := newMatrix(6, 6)
	m2 := newMatrix(6, 6)

	m1.fill()
	m2.fill()

	res1, _ := m1.mult(m2, Standart)
	res2, _ := m1.mult(m2, Winograd)
	res3, _ := m1.mult(m2, ModWinograd)

	fmt.Println("Стандартный алгоритм умножения матриц")
	res1.print()
	fmt.Println("Алгоритм умножения Винограда")
	res2.print()
	fmt.Println("Оптимизированный алгоритм умножения Винограда")
	res3.print()

	if !(comp(res1, res2) && comp(res1, res3)){
		t.Error("matrix arent right")
	}
}

func TestOddRows(t *testing.T){
	m1 := newMatrix(5, 5)
	m2 := newMatrix(5, 5)

	m1.fill()
	m2.fill()

	res1, _ := m1.mult(m2, Standart)
	res2, _ := m1.mult(m2, Winograd)
	res3, _ := m1.mult(m2, ModWinograd)

	fmt.Println("Стандартный алгоритм умножения матриц")
	res1.print()
	fmt.Println("Алгоритм умножения Винограда")
	res2.print()
	fmt.Println("Оптимизированный алгоритм умножения Винограда")
	res3.print()

	if !(comp(res1, res2) && comp(res1, res3)){
		t.Error("matrix arent right")
	}
}

func TestNonSquare(t *testing.T){
	m1 := newMatrix(2, 7)
	m2 := newMatrix(7, 4)

	m1.fill()
	m2.fill()

	res1, _ := m1.mult(m2, Standart)
	res2, _ := m1.mult(m2, Winograd)
	res3, _ := m1.mult(m2, ModWinograd)

	fmt.Println("Стандартный алгоритм умножения матриц")
	res1.print()
	fmt.Println("Алгоритм умножения Винограда")
	res2.print()
	fmt.Println("Оптимизированный алгоритм умножения Винограда")
	res3.print()

	if !(comp(res1, res2) && comp(res1, res3)){
		t.Error("matrix arent right")
	}
}

//func TestWinograd(t *testing.T) {
//	m1 := newMatrix(4, 3)
//	m2 := newMatrix(3, 5)
//	out := newMatrix(4, 5)
//
//	m1.matrix[0] = []int{1, 2, 3}
//	m1.matrix[1] = []int{4, 5, 6}
//	m1.matrix[2] = []int{7, 8, 9}
//	m1.matrix[3] = []int{10, 11, 12}
//
//
//	m2.matrix[0] = []int{5, 6, 7, 8, 9}
//	m2.matrix[1] = []int{1, 2, 3, 4, 5}
//	m2.matrix[2] = []int{10, 11, 20, 15, 14}
//
//	out.matrix[0] = []int{37, 43, 73, 61, 61}
//	out.matrix[1] = []int{85, 100, 163, 142, 145}
//	out.matrix[2] = []int{133, 157, 253, 223, 229}
//	out.matrix[3] = []int{181, 214, 343, 304, 313}
//
//	ans := Winograd(m1, m2)
//
//	if !comp(ans, out){
//		t.Error("matrix arent right")
//	}
//}
//
//func TestStandart(t *testing.T) {
//	m1 := newMatrix(4, 3)
//	m2 := newMatrix(3, 5)
//	out := newMatrix(4, 5)
//
//	m1.matrix[0] = []int{1, 2, 3}
//	m1.matrix[1] = []int{4, 5, 6}
//	m1.matrix[2] = []int{7, 8, 9}
//	m1.matrix[3] = []int{10, 11, 12}
//
//
//	m2.matrix[0] = []int{5, 6, 7, 8, 9}
//	m2.matrix[1] = []int{1, 2, 3, 4, 5}
//	m2.matrix[2] = []int{10, 11, 20, 15, 14}
//
//	out.matrix[0] = []int{37, 43, 73, 61, 61}
//	out.matrix[1] = []int{85, 100, 163, 142, 145}
//	out.matrix[2] = []int{133, 157, 253, 223, 229}
//	out.matrix[3] = []int{181, 214, 343, 304, 313}
//
//	ans := Standart(m1, m2)
//
//	if !comp(ans, out){
//		t.Error("matrix arent right" )
//	}
//}
//
//func TestModWinograd(t *testing.T) {
//	m1 := newMatrix(4, 3)
//	m2 := newMatrix(3, 5)
//	out := newMatrix(4, 5)
//
//	m1.matrix[0] = []int{1, 2, 3}
//	m1.matrix[1] = []int{4, 5, 6}
//	m1.matrix[2] = []int{7, 8, 9}
//	m1.matrix[3] = []int{10, 11, 12}
//
//
//	m2.matrix[0] = []int{5, 6, 7, 8, 9}
//	m2.matrix[1] = []int{1, 2, 3, 4, 5}
//	m2.matrix[2] = []int{10, 11, 20, 15, 14}
//
//	out.matrix[0] = []int{37, 43, 73, 61, 61}
//	out.matrix[1] = []int{85, 100, 163, 142, 145}
//	out.matrix[2] = []int{133, 157, 253, 223, 229}
//	out.matrix[3] = []int{181, 214, 343, 304, 313}
//
//	ans := ModWinograd(m1, m2)
//
//	if !comp(ans, out){
//		t.Error("matrix arent right" )
//	}
//}
//
//func TestWinCon(t *testing.T) {
//	m1 := newMatrix(4, 3)
//	m2 := newMatrix(3, 5)
//	out := newMatrix(4, 5)
//
//	m1.matrix[0] = []int{1, 2, 3}
//	m1.matrix[1] = []int{4, 5, 6}
//	m1.matrix[2] = []int{7, 8, 9}
//	m1.matrix[3] = []int{10, 11, 12}
//
//
//	m2.matrix[0] = []int{5, 6, 7, 8, 9}
//	m2.matrix[1] = []int{1, 2, 3, 4, 5}
//	m2.matrix[2] = []int{10, 11, 20, 15, 14}
//
//	out.matrix[0] = []int{37, 43, 73, 61, 61}
//	out.matrix[1] = []int{85, 100, 163, 142, 145}
//	out.matrix[2] = []int{133, 157, 253, 223, 229}
//	out.matrix[3] = []int{181, 214, 343, 304, 313}
//
//	ans := WinCon(m1, m2)
//
//	if !comp(ans, out){
//		t.Error("matrix arent right" )
//	}
//}
//
//
//func TestCon(t *testing.T) {
//	m1 := newMatrix(4, 3)
//	m2 := newMatrix(3, 5)
//	out := newMatrix(4, 5)
//
//	m1.matrix[0] = []int{1, 2, 3}
//	m1.matrix[1] = []int{4, 5, 6}
//	m1.matrix[2] = []int{7, 8, 9}
//	m1.matrix[3] = []int{10, 11, 12}
//
//
//	m2.matrix[0] = []int{5, 6, 7, 8, 9}
//	m2.matrix[1] = []int{1, 2, 3, 4, 5}
//	m2.matrix[2] = []int{10, 11, 20, 15, 14}
//
//	out.matrix[0] = []int{37, 43, 73, 61, 61}
//	out.matrix[1] = []int{85, 100, 163, 142, 145}
//	out.matrix[2] = []int{133, 157, 253, 223, 229}
//	out.matrix[3] = []int{181, 214, 343, 304, 313}
//
//	ans := Con(m1, m2)
//
//	if !comp(ans, out){
//		t.Error("matrix arent right" )
//	}
//}