package main

import (
	"fmt"
	"os"
	"time"
)

func (m Matrix) measure(factor Matrix, times int, f method) int64{
	var total time.Duration
	for i := 0; i < times; i++{
		start := time.Now()
		f(m, factor)
		total += time.Since(start)
	}
	if times == 0{
		return 0
	}

	return total.Microseconds() / int64(times)
}

func measureAll(funcs map[string]method){
	var amount int
	fmt.Print("Enter dimension of a square matrix ->> ")
	if amount, err := fmt.Scanln(&amount); amount == 0 || err != nil{
		fmt.Println("Wrong input")
		return
	}
	fmt.Println()

	if amount > 400{
		fmt.Println("Too big number to calculate")
		return
	}

	matrix1 := newMatrix(amount, amount)
	matrix2 := newMatrix(amount, amount)

	matrix1.fill()
	matrix2.fill()

	const times = 5
	for key, val := range funcs{
		fmt.Println(key, "worked for", matrix1.measure(matrix2, times, val), "microseconds")
	}

}

func createFiles(funcs map[string]method){
	files := map[int]map[string]string{
		0: {
			simple: "simple.txt",
			vinogradov: "vinogradov.txt",
			improvedVinogradov: "modvinograd.txt",
		},
		1: {
			simple: "simple1.txt",
			vinogradov: "vinogradov1.txt",
			improvedVinogradov: "modvinograd1.txt",
		},
	}
	
	const(
		start = 100
		end = 1000
		times = 2
		step = 100
	)


	//wg := &sync.WaitGroup{}
	for index := 0; index < 2; index++{
		fmt.Println("index = ", index)
		for i := start + index; i <= end + index; i += step{
			fmt.Println("curr dimension", i)
			matr1 := newMatrix(i, i)
			matr2 := newMatrix(i, i)

			matr1.fill()
			matr2.fill()

			//fmt.Println("here", files[index])
			for key, file := range files[index]{
				//wg.Add(1)
				func(key, file string) {
					res := matr1.measure(matr2, times, funcs[key])
					f, err := os.OpenFile(file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
					if err != nil {
						fmt.Println(err)
					}
					if _, err := f.WriteString(fmt.Sprint(i, res, "\n")); err != nil {
						fmt.Println(err)
					}
					if err := f.Close(); err != nil {
						fmt.Println(err)
					}
					//fmt.Println("done", res)
					//wg.Done()
				}(key, file)
			}

			//wg.Wait()
		}
	}
}
