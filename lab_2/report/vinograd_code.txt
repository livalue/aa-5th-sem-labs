func Winograd(m, factor Matrix)Matrix{
	output := newMatrix(m.rows, factor.cols)
	mulH := make([]int, m.rows)
	mulV := make([]int, factor.cols)

	for i := 0; i < m.rows; i++{
		for j := 0; j < m.cols / 2; j++{
			mulH[i] = mulH[i] + m.matrix[i][2*j] * m.matrix[i][2*j + 1]
		}
	}

	for i := 0; i < factor.cols; i++{
		for j := 0; j < m.cols / 2; j++{
			mulV[i] = mulV[i] + factor.matrix[2*j][i] * factor.matrix[2*j + 1][i]
		}
	}

	for i := 0; i < m.rows; i++{
		for j := 0; j < factor.cols; j++{
			output.matrix[i][j] = -mulH[i] - mulV[j]
			for k := 0; k < m.cols / 2; k++{
				output.matrix[i][j] = output.matrix[i][j] + (m.matrix[i][2*k] + factor.matrix[2*k + 1][j]) *
					(m.matrix[i][2*k + 1] + factor.matrix[2*k][j])
			}
		}
	}

	if m.cols % 2 == 1{
		for i := 0; i < m.rows; i++{
			for j := 0; j < factor.cols; j++{
				output.matrix[i][j] = output.matrix[i][j] + m.matrix[i][m.cols - 1] * factor.matrix[m.cols - 1][j]
			}
		}
	}

	return output
}