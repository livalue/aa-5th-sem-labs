package main

import "testing"

func TestZero(t *testing.T) {
	arr := make(list, 0)

	res1 := make(list, 0)
	res2 := make(list, 0)
	res3 := make(list, 0)

	res1.copy(arr)
	res2.copy(arr)
	res3.copy(arr)

	BubbleSort(res1)
	InsertionSort(res2)
	Quicksort(res3)

	if res1.equals(res2) == false || res2.equals(res3) == false{
		t.Error("Did not pass")
	}
}

func TestOneSize(t *testing.T){
	arr := make(list, 1)
	arr.fill()

	res1 := make(list, 1)
	res2 := make(list, 1)
	res3 := make(list, 1)

	res1.copy(arr)
	res2.copy(arr)
	res3.copy(arr)

	BubbleSort(res1)
	InsertionSort(res2)
	Quicksort(res3)

	println("After bubble sort:")
	res1.print()
	println("After insertion sort:")
	res2.print()
	println("After quick sort:")
	res3.print()

	if res1.equals(res2) == false || res2.equals(res3) == false{
		t.Error("Did not pass")
	}
}

func TestTwenty(t *testing.T){
	arr := make(list, 20)
	arr.fill()

	res1 := make(list, 20)
	res2 := make(list, 20)
	res3 := make(list, 20)

	res1.copy(arr)
	res2.copy(arr)
	res3.copy(arr)

	BubbleSort(res1)
	InsertionSort(res2)
	Quicksort(res3)

	println("After bubble sort:")
	res1.print()
	println("After insertion sort:")
	res2.print()
	println("After quick sort:")
	res3.print()

	if res1.equals(res2) == false || res2.equals(res3) == false{
		t.Error("Did not pass")
	}
}

func TestTwentyone(t *testing.T){
	arr := make(list, 21)
	arr.fill()

	res1 := make(list, 21)
	res2 := make(list, 21)
	res3 := make(list, 21)

	res1.copy(arr)
	res2.copy(arr)
	res3.copy(arr)

	BubbleSort(res1)
	InsertionSort(res2)
	Quicksort(res3)

	println("After bubble sort:")
	res1.print()
	println("After insertion sort:")
	res2.print()
	println("After quick sort:")
	res3.print()

	if res1.equals(res2) == false || res2.equals(res3) == false{
		t.Error("Did not pass")
	}
}
