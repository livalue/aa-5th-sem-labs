package main

import (
	"errors"
	"fmt"
)

func input(funcs map[string]sort){
	var e error

	defer func() {
		if e != nil{
			fmt.Println(e)
		}
	}()

	var length, amount, choice int
	fmt.Print("Введите размерность массива ->> ")
	amount, e = fmt.Scanln(&length)
	if amount == 0 || e != nil{
		return
	}

	fmt.Print(
		"Выберите как вы хотите заполнить массив:\n",
		"1 - Вручную\n",
		"2 - Рандомно\n",
		"Ваш выбор ->>",
		)

	amount, e = fmt.Scanln(&choice)
	if amount == 0 || e != nil{
		return
	}

	var arr list
	switch choice {
	case 1:
		arr = make(list, length)
		e = arr.input()
		if e != nil{
			return
		}
	case 2:
		arr = make(list, length)
		arr.fill()
	default:
		e = errors.New("Неверный выбор")
		return
	}

	fmt.Println("Исходный массив:")
	arr.print()

	fmt.Print(
		"Выберите сортировку:\n",
		"1 - Сортировка пузырьком\n",
		"2 - Сортировка вставками\n",
		"3 - Быстрая сортировка\n",
		"Ваш выбор ->> ",
		)
	amount, e = fmt.Scanln(&choice)
	if amount == 0 || e != nil{
		return
	}

	var str string
	switch choice {
	case 1:
		str = buble
	case 2:
		str = insert
	case 3:
		str = quick
	}

	funcs[str](arr)

	fmt.Println("После сортировки:")

	arr.print()

}
