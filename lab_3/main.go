package main

import (
	"errors"
	"fmt"
	"math"
	"math/rand"
	"time"
)

type list []int
type sort func (list)
const(
	buble = "Сортировка пузырьком"
	insert = "Сортировка вставками"
	quick = "Быстрая сортировка"
)

func (arr list) equals(temp list)bool{
	if len(temp) != len(arr){
		return false
	}

	for i := 0 ; i < len(arr); i++{
		if arr[i] != temp[i]{
			return false
		}
	}

	return true
}
func (arr list) fill(){

	seed := rand.NewSource(time.Now().UnixNano())
	random := rand.New(seed)

	for i := 0; i < len(arr); i++{
		arr[i] = random.Intn(1001) - 500
	}
}

func (arr list) print(){
	for _, elem := range arr{
		fmt.Print(elem, " ")
	}
	fmt.Println()
}

func (arr list) input() error{
	for i := range arr{
		fmt.Print("Введите число ->> ")
		if amount, err := fmt.Scanln(&arr[i]); amount == 0 || err != nil{
			return errors.New("Некорректный ввод")
		}
	}
	return nil
}

func (arr list) measure(method sort, times int)int64{
	var total int64
	for i := 0; i < times; i++{
		start := time.Now()
		method(arr)
		total += time.Since(start).Milliseconds()
	}
	if times == 0{
		return 0
	}
	return total / int64(times)
}

func (arr list) copy(src list){
	max := int(math.Max(float64(len(arr)), float64(len(src))))
	for i := 0 ; i < max ; i++{
		arr[i] = src[i]
	}
}

func main(){
	funcs := map[string]sort{
		buble: BubbleSort,
		insert: InsertionSort,
		quick: Quicksort,
	}

	var ans int
	flag := true

	for flag {
		fmt.Print(
			"########################MENU########################\n\n",
			"1 - Проверка работы\n",
			"2 - Замер времени работы\n",
			//"3 - Meausure for graph\n",
			"0 - Выход\n\n",
			"Ваш выбор ->> ",
		)
		if amount, err := fmt.Scanln(&ans); amount == 0 || err != nil {
			fmt.Println("Некорректный ввод")
			continue
		}

		switch ans {
		case 1:
			input(funcs)
		case 2:
			measureAll(funcs)
		case 3:
			createFiles(funcs)
		case 0:
			flag = false
		default:
			fmt.Println("Неверный выбор")
		}
	}

}
