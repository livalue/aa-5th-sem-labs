package main

import (
	"fmt"
	"os"
	"sync"
)

func measureAll(funcs map[string]sort){
	var length int
	fmt.Print("Введите размерность массива ->> ")
	if amount, err := fmt.Scanln(&length); amount == 0 || err != nil{
		fmt.Println("Некорректный ввод")
		return
	}

	fmt.Println()
	arr := make(list, length)
	arr.fill()
	const times = 10
	for key, val := range funcs{
		temp := make(list, length)
		temp.copy(arr)
		fmt.Println(key, "отработала за", temp.measure(val, times))
	}
	fmt.Println()
}


func createFiles(funcs map[string]sort){
	files := map[string]string{
		buble: "bubble.txt",
		insert: "insert.txt",
		quick: "quick.txt",
	}

	const(
		start = 20000
		end = 45000
		times = 1
		step = 5000
	)

	wg := &sync.WaitGroup{}
	for i := start; i <= end; i += step{
		arr := make(list, i)
		//for j:= 0; j < i; j++ {
		//	arr[j] = i - j
		//}
		arr.fill()
		//fmt.Println("here", files[index])
		for key, file := range files{
			wg.Add(1)
			go func(key, file string) {
				temp := make(list, i)
				temp.copy(arr)
				res := temp.measure(funcs[key],times)
				f, err := os.OpenFile(file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
				if err != nil {
					fmt.Println(err)
				}
				if _, err := f.WriteString(fmt.Sprint(i, res, "\n")); err != nil {
					fmt.Println(err)
				}
				if err := f.Close(); err != nil {
					fmt.Println(err)
				}
				//fmt.Println("done", res)
				wg.Done()
			}(key, file)
		}
		wg.Wait()
	}
}
