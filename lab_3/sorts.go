package main

func InsertionSort(arr list) {
	for i := 1; i < len(arr); i++ {
		x := arr[i]
		j := i
		for ; j >= 1 && arr[j-1] > x; j-- {
			arr[j] = arr[j-1]
		}
		arr[j] = x
	}
}

//func Quicksort(a list){
//	if len(a) < 2 {
//		return
//	}
//
//	left, right := 0, len(a)-1
//
//	pivot := rand.Int() % len(a)
//
//	a[pivot], a[right] = a[right], a[pivot]
//
//	for i := range a {
//		if a[i] < a[right] {
//			a[left], a[i] = a[i], a[left]
//			left++
//		}
//	}
//
//	a[left], a[right] = a[right], a[left]
//
//	Quicksort(a[:left])
//	Quicksort(a[left+1:])
//
//	return
//}

func swap(arr list, i, j int) {
	arr[i], arr[j] = arr[j], arr[i]
}

func BubbleSort(arr list) {
	for i := 0; i < len(arr); i++ {
		for j := i; j < len(arr); j++ {
			if arr[i] > arr[j] {
				swap(arr, i, j)
			}
		}
	}
}

func Partition(arr list, lhs, rhs int) int{
	less := lhs

	curr := arr[rhs]
	for i := lhs; i < rhs; i++{
		if arr[i] <= curr{
			swap(arr, i, less)
			less++
		}
	}
	swap(arr, less, rhs)
	return less
}

func quickSort(arr list,left int,right int){

	if left < right{
		sup := Partition(arr, left, right)
		quickSort(arr, left, sup - 1)
		quickSort(arr, sup + 1, right)
	}
	return
}

func Quicksort(arr list){
	quickSort(arr, 0, len(arr) - 1)
}

