cmake_minimum_required(VERSION 3.17)
project(lab_4)

#set(CMAKE_CXX_STANDARD 17)
set(SOURCES
        main.cpp
        )
set(HEADERS
        matrix.hpp
        )
add_executable(app ${SOURCES} ${HEADERS})

set_target_properties(
        app PROPERTIES
        CXX_STANDARD 17
        CXX_STANDARD_REQUIRED ON
        COMPILE_OPTIONS "-Wall;-Werror;-O0"
)