//
// Created by dimon on 28.10.2020.
//

#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include "matrix.hpp"

template<typename T>
void measureSimple(size_t start, size_t end, size_t step, size_t times){
    size_t total = 0;
    std::ofstream output("simple.txt");
    for (size_t i = start; i <= end; i += step){
        Matrix<T> matr(i, i), factor(i, i);
        matr.fill();
        factor.fill();
        total = 0;
        for (size_t j = 0; j < times; j++){
            std::cout << i << " " << j << std::endl;
            auto start_t = std::chrono::steady_clock::now();
            matr.multFirst(factor);
            auto end_t = std::chrono::steady_clock::now();
            total += std::chrono::duration_cast<std::chrono::microseconds>(end_t - start_t).count();
        }
        output << i << " " << (times != 0 ? total / times : 0) << std::endl;
    }

}
template <typename T>
void measure(size_t start, size_t end, size_t step, size_t times){
    size_t total = 0;
    std::ofstream out;
    std::map<std::string, func<T>> hash{
            {"row_blocks", &Matrix<T>::multByRows},
            {"row_strings", &Matrix<T>::multByRow}
    };
    std::vector<size_t> workers_amount{1, 2, 4, 8, 16};

    for (size_t i = start; i <= end; i += step){
        Matrix<T> matr(i, i), factor(i, i);
        matr.fill();
        factor.fill();
        for (auto &amount: workers_amount){
            for(auto& [file, f]: hash){
                std::cout << file << " " << i << " " << amount << std::endl;
                total = 0;
                for (size_t j = 0; j < times; j++){
                    auto start_t = std::chrono::steady_clock::now();
                    (matr.*f)(factor, amount);
                    auto end_t = std::chrono::steady_clock::now();
                    total += std::chrono::duration_cast<std::chrono::microseconds>(end_t - start_t).count();
                }
                out.open(file + std::to_string(amount) + ".txt", std::ios_base::app);
                out << i << " " << (times != 0 ? total / times: 0) << std::endl;
                out.close();
            }
        }
    }


}

void measure(){
    size_t dimension, workers;
    const int times = 10;
    std::cout << "Enter dimension of matrixes ->> ";
    if (!(std::cin >> dimension)){
        std::cout << "Wrong input" << std::endl;
        return;
    }
    std::cout << std::endl;

    std::cout << "Enter amount of workers ->> ";
    if (!(std::cin >> workers) or workers <= 0){
        std::cout << "Wrong input" << std::endl;
        return;
    }
    std::cout << std::endl;

    Matrix<int> matr(dimension, dimension), factor(dimension, dimension);
    matr.fill();
    factor.fill();

    std::map<std::string, func<int>> funcs{
            {"ByRowGroups method", &Matrix<int>::multByRows},
            {"ByRowsStrings method", &Matrix<int>::multByRow}
    };

    size_t total = 0;
    for (size_t i = 0; i < times; i++){
        auto start = std::chrono::steady_clock::now();
        matr.multFirst(factor);
        auto end = std::chrono::steady_clock::now();
        total += std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
    }

    std::cout << "Simple method worked for " << (times != 0 ? total / times : 0) << std::endl;

    for (auto & [name, method]: funcs){
        total = 0;
        for (size_t i = 0; i < times; i++){
            auto start = std::chrono::steady_clock::now();
            (matr.*method)(factor, workers);
            auto end = std::chrono::steady_clock::now();
            total += std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
        }
        std::cout << name << " " << "worked for " << (times != 0 ? total / times : 0) << std::endl;
    }
}

void checkWork(){
    size_t rows1, cols1, rows2, cols2, choice = 1, workers;

    std::cout << "Enter amount of rows for first matrix ->> ";
    if (!(std::cin >> rows1)){
        std::cout << "Wrong data" << std::endl;
        return;
    }
    std::cout << std::endl;

    std::cout << "Enter amount of cols for first matrix ->> ";
    if (!(std::cin >> cols1)){
        std::cout << "Wrong data" << std::endl;
        return;
    }
    std::cout << std::endl;

    std::cout << "Enter amount of rows for second matrix ->> ";
    if (!(std::cin >> rows2)){
        std::cout << "Wrong data" << std::endl;
        return;
    }
    std::cout << std::endl;

    std::cout << "Enter amount of cols for second matrix ->> ";
    if (!(std::cin >> cols2)){
        std::cout << "Wrong data" << std::endl;
        return;
    }
    std::cout << std::endl;

    if (cols1 != rows2){
        std::cout << "Wrong dimension" << std::endl;
        return;
    }
    std::cout << "Enter amount of workers ->> ";
    if (!(std::cin >> workers) or workers <= 0){
        std::cout << "Wrong input" << std::endl;
        return;
    }

    std::cout << std::endl;

    Matrix<int> m1(rows1, cols1), m2(rows2, cols2);
    while (choice){
        std::printf(
                "Choose fill method:\n"
                "1 - Manual input\n"
                "2 - Random\n"
                "Your choice ->>"
        );
        if (!(std::cin >> choice)){
            std::cin.clear(); //clear bad input flag
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Incorrect data" << std::endl;
            continue;
        }

        switch (choice) {
            case 1:
                if (!m1.input() or !m2.input()){
                    std::cin.clear(); //clear bad input flag
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    std::cout << "Wrong data";
                }
                break;
            case 2:
                m1.fill();
                m2.fill();
                break;
            default:
                std::cout << "Wrong choice" << std::endl;
                continue;

        }

        std::cout << "First matrix:" << std::endl;
        m1.print();
        std::cout << "Second matrix:" << std::endl;
        m2.print();

        choice = 1;
        Matrix<int> output(rows1, cols2);
        std::cout << std::endl;
        while(choice){
            printf(
                    "Choose method:\n"
                    "1 - Simple method\n"
                    "2 - By row blocks method\n"
                    "3 - By cols method\n"
                    "0 - Exit\n"
                    "Your choice ->> "
                    );

            if (!(std::cin >> choice)){
                std::cin.clear(); //clear bad input flag
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << "Wrong data" << std::endl;
                choice = 0;
                continue;
            }

            switch (choice) {
                case 1:
                    output = m1.multFirst(m2);
                    break;
                case 2:
                    output = m1.multByRows(m2, workers);
                    break;
                case 3:
                    output = m1.multByRow(m2, workers);
                    break;
                case 0:
                    break;
                default:
                    std::cout << "Wrong choice";
                    continue;
            }

            if (choice){
                std::cout << "Answer:" << std::endl;
                output.print();
            }
        }
        choice = 0;
    }
}

bool test(size_t r1, size_t c1, size_t r2, size_t c2, size_t workers){
    Matrix<int> matr(r1, c1), factor(r2, c2), out1(r1, c2),
            out2(r1, c2), out3(r1, c2);

    matr.fill();
    factor.fill();

    out1 = matr.multFirst(factor);
    out2 = matr.multByRows(factor, workers);
    out3 = matr.multByRow(factor, workers);

    std::cout << "Simple multiplication:" << std::endl;
    out1.print();

    std::cout << "First parallel:" << std::endl;
    out2.print();

    std::cout << "Second parallel:" << std::endl;
    out3.print();

    return (out1.equals(out2) and out2.equals(out3));
}

void tests(){

    std::cout << (test(2, 2, 2, 2, 2) ? "PASS" : "ERROR") << std::endl;
    std::cout << std::endl;
    std::cout << (test(1, 1, 1, 1, 1) ? "PASS" : "ERROR") << std::endl;
    std::cout << std::endl;
    std::cout << (test(6, 6, 6, 6, 4) ? "PASS" : "ERROR") << std::endl;
    std::cout << std::endl;
    std::cout << (test(5, 5, 5, 5, 4) ? "PASS" : "ERROR") << std::endl;
    std::cout << std::endl;
    std::cout << (test(2, 7, 7, 4, 4) ? "PASS" : "ERROR") << std::endl;
    std::cout << std::endl;

}

int main() {
    int choice = 1;
    while (choice){
        std::cout <<
                "################################MENU################################\n"
                << "1 - Check work\n"
                << "2 - Measure time\n"
                << "0 - Exit\n"
                << "Your choice ->>";
        ;

        if (!(std::cin >> choice)){
            std::cin.clear(); //clear bad input flag
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Wrong input"<<std::endl;
            choice = 1;
            continue;
//            return 0;
        }

        switch (choice) {
            case 1:
                checkWork();
                break;
            case 2:
                measure();
                break;
            case 0:
                break;
            default:
                std::cout << "Wrong section" << std::endl;
        }
    }

//    measure<int>(100, 1000, 100, 2);
//    measureSimple<int>(100, 1000, 100, 2);
    return 0;
}