//
// Created by dimon on 28.10.2020.
//

#ifndef LAB_4_MATRIX_H
#define LAB_4_MATRIX_H
#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <queue>
#include <random>
#include <mutex>
#include <atomic>
//#include <condition_variable>

template <typename T>
class Matrix;

template<typename T>
using func = Matrix<T> (Matrix<T>::*)(const Matrix<T>&, size_t);

template<typename T>
class Matrix {
public:
    Matrix() = delete;
    Matrix(size_t rows_, size_t cols_);

    T*& operator [](size_t  index)const;

    Matrix<T>& operator = (const Matrix<T>& temp);

    size_t rows()const{
        return rows_;
    }

    size_t cols()const{
        return cols_;
    }

    bool input();

    void print()const;

    void fill();

    bool equals(const Matrix<T>&)const;

    Matrix<T> multFirst(const Matrix<T>&);

    Matrix<T> multByRows(const Matrix<T>&, size_t);

    Matrix<T> multByBlocks(const Matrix<T>&, size_t);

    Matrix<T> multByRow(const Matrix<T>&, size_t);

//    Matrix<T> mult(const Matrix<T>& factor){
//
//    }

    ~Matrix(){
        for (size_t i = 0; i < this->rows_; i++)
            delete matrix[i];
        delete matrix;
    }
private:
    T** matrix;
    size_t rows_{};
    size_t cols_{};

    using tuple = struct data{
        data(size_t rs, size_t re, size_t cs, size_t ce):row_start{rs}, row_end{re}, col_start{cs}, col_end{ce}{};
        size_t row_start;
        size_t row_end;
        size_t col_start;
        size_t col_end;
    };

};


#endif //LAB_4_MATRIX_H
