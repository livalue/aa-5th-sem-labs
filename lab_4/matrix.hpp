//
// Created by dimon on 28.10.2020.
//

#include "matrix.h"

template <typename T>
Matrix<T>::Matrix(size_t rows_, size_t cols_){
    matrix = new T*[rows_];
    for (size_t i = 0; i < rows_; i++){
        matrix[i] = new T[cols_];
        for (size_t j = 0; j < cols_; j++)
            matrix[i][j] = 0;
    }
    this->rows_ = rows_;
    this->cols_ = cols_;
}

template <typename T>
T*& Matrix<T>::operator[](size_t index)const{
    return this->matrix[index];
}

template <typename T>
void Matrix<T>::print() const{
    for (size_t i = 0; i < this->rows_; i++){
        for (size_t j = 0; j < this->cols_; j++)
            std::cout << this->matrix[i][j] << " ";
        std::cout << std::endl;
    }
}

template <typename T>
bool Matrix<T>::input() {
    for (size_t i = 0; i < this->rows_; i++){
        std::cout << "Enter elems for " << i + 1 << " row" << std::endl;
        for (size_t j = 0; j < this->cols_; j++)
            if (!(std::cin >> this->matrix[i][j]))
                return false;
        std::cout << std::endl;
    }
    return true;
}

template<typename T>

Matrix<T> Matrix<T>::multFirst(const Matrix<T>& factor) {
    Matrix<T> output(rows_, factor.cols());
    for (size_t i = 0; i < rows_; i++){
        for (size_t j = 0; j < factor.cols(); j++) {
            for (size_t k = 0; k < cols_; k++)
                output.matrix[i][j] += matrix[i][k] * factor[k][j];
        }
    }

    return output;
}

template <typename T>
Matrix<T> Matrix<T>::multByBlocks(const Matrix<T> &factor, size_t workers_amount) {
    std::vector<std::thread> workers;
    std::queue<tuple> tuples;
    Matrix<T> output(rows_, factor.cols());
    std::mutex m;

    auto func = [&](){
        while(!tuples.empty()){
            m.lock();
            auto temp = tuples.front();
            tuples.pop();
            m.unlock();
            for (size_t i = temp.row_start; i < temp.row_end; i++){
                for (size_t j = temp.col_start; j < temp.col_end; j++){
                    for(size_t k = 0; k < cols_; k++)
                        output[i][j] += matrix[i][k] * factor[k][j];
                }
            }
        }
    };

    size_t step_row = static_cast<size_t>(ceil(static_cast<double>(rows_) / workers_amount));
    size_t step_col = static_cast<size_t>(ceil(static_cast<double>(factor.cols()) / workers_amount));

    for (size_t i = 0, r_s = 0, r_e = step_row; i < workers_amount; i++, r_s += step_row, r_e += step_row){
        if (r_e >= rows_){
            r_e = rows_;
            i = workers_amount;
        }
        for (size_t j = 0, c_s = 0, c_e = step_col; j < workers_amount; j++, c_s += step_col, c_e += step_col){
            if (c_e >= factor.cols()){
                c_e = factor.cols();
                j = workers_amount;
            }
            tuples.push(tuple{r_s, r_e, c_s, c_e});
        }
    }

    for (size_t i = 0; i < workers_amount; i++)
        workers.push_back(std::thread(func));

    for (auto& worker: workers)
        worker.join();

    return output;
}

template <typename T>
Matrix<T> Matrix<T>::multByRows(const Matrix<T> &factor, size_t workers_amount) {
    Matrix<T> output(rows_, factor.cols());

    std::vector<std::thread> workers;

    auto func = [&](size_t start, size_t end){
        for (size_t i = start; i < end; i++){
            for (size_t j = 0; j < factor.cols(); j++){
                for(size_t k = 0; k < cols_; k++)
                    output[i][j] += matrix[i][k] * factor[k][j];
            }
        }
    };

    size_t workers_in_process = 0;
    auto step = static_cast<size_t>(ceil(static_cast<double>(rows_) / workers_amount));
    for (size_t i = 0, start = 0, end = step; i < workers_amount; i++, start += step, end+=step, workers_in_process++){
        if (end >= rows_){
            end = rows_;
            i = workers_amount;
        }
        workers.push_back(std::thread(func, start, end ));
    }
    for (auto& worker: workers)
        worker.join();
    return output;
}

template <typename T>
Matrix<T> Matrix<T>::multByRow(const Matrix<T> &factor, size_t workers_amount) {
    Matrix<T> output(rows_, factor.cols());
    std::queue<size_t> indexes;
    std::vector<std::thread> workers;
    std::mutex m;

    auto func = [&](){
        bool flag = true;
        while(flag){
            m.lock();
            if (indexes.empty()){
                flag = false;
                m.unlock();
                continue;
            }
            auto index = indexes.front();
            indexes.pop();
            m.unlock();
            for (size_t j = 0; j < factor.cols(); j++){
                for(size_t k = 0; k < cols_; k++)
                    output[index][j] += matrix[index][k] * factor[k][j];
            }
        }
    };

    for (size_t i = 0; i < rows_; i++)
        indexes.push(i);

    for (size_t i = 0; i < workers_amount; i++)
        workers.push_back(std::thread(func));

    for (auto& worker: workers)
        worker.join();

    return output;
}

template <typename T>
void Matrix<T>::fill() {
    std::default_random_engine generator(std::chrono::steady_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<int> distribution(0, 10);
    for (size_t i = 0; i < rows_; i++)
        for (size_t j = 0; j < cols_; j++)
            matrix[i][j] = T(distribution(generator));
}

template <typename T>
Matrix<T>& Matrix<T>::operator=(const Matrix<T> &temp) {
    rows_ = rows();
    cols_ = cols();
    matrix = new T*[rows_];
    for (size_t i = 0; i < rows_; i++){
        matrix[i] = new T[cols_];
        for (size_t j = 0; j < cols_; j++)
            matrix[i][j] = temp[i][j];
    }

    return *this;
}

template<typename T>
bool Matrix<T>::equals(const Matrix<T> &matr) const {
    if (rows_ != matr.rows() || cols_ != matr.cols())
        return false;

    for (size_t i = 0; i < rows_; i++)
        for (size_t j = 0; j < cols_; j++)
            if (matrix[i][j] != matr[i][j])
                return false;
    return true;
}