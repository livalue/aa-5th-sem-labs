package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
)

func gen_string(l int)string{
	var chars = "qwertyuiopasdfghjklzxcvbnm1234567890"

	var out []byte
	for i := 0; i < l; i++{
		out = append(out, chars[rand.Intn(l)])
	}
	return string(out)
}

func create_file_name(file_name string) string{
	return fmt.Sprintf("%s%c%s.txt", *path, os.PathSeparator, file_name)
}

func generate_random_file(file_name string){
	file, err := os.Create(create_file_name(file_name))
	if err != nil{
		fmt.Println("error occured in creating file - ", err)
	}
	buffer := bufio.NewWriter(file)
	for i:= 0; i < 60000; i++ {
		_, err = buffer.WriteString(gen_string(rand.Intn(20)+10) + "\n")
		if err != nil {
			log.Fatal("error occured while writing in buffer - ", err)
		}
	}
	file.Close()
}

var path *string

func main() {
	path = flag.String("path", "./next", "path to files")
	amount := flag.Int("amount", 0, "amount of files")
	flag.Parse()

	for i := 0; i < *amount; i++{
		generate_random_file(fmt.Sprintf("%d", i + 1))
	}


}
