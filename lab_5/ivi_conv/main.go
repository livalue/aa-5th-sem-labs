package main

import (
	"fmt"
	"golang.org/x/net/html"
	"net/http"
	"strings"
	"time"
)

type ws func(chan interface{}, chan interface{})

type movie struct{
	name string
	rating_int string
	rating_float string
	description string
	duration string
}

type chunk struct{
	film *movie
	node *html.Node
}

func findInfo(n *html.Node, film *movie){
	if n.Type == html.ElementNode && n.Data == "div" && len(n.Attr) != 0{
		switch n.Attr[0].Val {
		case "parameters__info":
			for a := n.FirstChild; a != nil; a = a.NextSibling{
				if a.Type == html.ElementNode{
					film.description += a.FirstChild.Data
				}else{
					film.description += a.Data
				}
			}
		case "nbl-ratingAmple__value":
			film.rating_int = n.FirstChild.FirstChild.Data
			span := n.FirstChild.NextSibling.LastChild
			film.rating_float = "," + span.Data
		case "iconedText__title":
			info := n.FirstChild.Data
			if strings.Index(info, "мин") != -1{
				film.duration = n.FirstChild.Data
			}
		}
	}

	for child := n.FirstChild; child != nil; child = child.NextSibling{
		findInfo(child, film)
	}
}

func getFilmPage(url string)*html.Node{
	resp, err := http.Get("https://www.ivi.ru/" + url)
	if err != nil{
		fmt.Println("err in fillInfo = ", err)
		return nil
	}
	defer resp.Body.Close()

	doc, err := html.Parse(resp.Body)
	if err != nil{
		fmt.Println("err in Parse html = ", err)
		return nil
	}
	return doc
	//return nil
}

func fillMovie(n *html.Node, film *movie, out chan interface{}, start time.Time){
	if n.Type == html.ElementNode && n.Data == "a"{
		node := getFilmPage(n.Attr[0].Val)
		fmt.Println("duration = ", time.Since(start))
		if node != nil{
			return
		}
		fmt.Println("duration = ", time.Since(start))
		out <- chunk{
			film: film,
			node: node,
		}
	}

	for child := n.FirstChild; child != nil; child = child.NextSibling{
		fillMovie(child, film, out, start)
	}
}

func getFilms(n *html.Node, out chan interface{}){
	if n.Type == html.ElementNode && n.Data == "li" {
		for _, atr := range n.Attr{
			if atr.Key == "class" && atr.Val == "gallery__item gallery__item_virtual"{
				fmt.Println("send")
				out <- n
				break
			}
		}

	}

	for c := n.FirstChild; c != nil; c = c.NextSibling{
		getFilms(c, out)
	}
}

func first(in, out chan interface{}){
	client := http.DefaultClient
	client.Timeout = time.Second * 15

	for link := range in{
		req, err := http.NewRequest("GET", link.(string), nil)
		if err != nil{
			fmt.Println("err = ", err)
		}

		req.Header.Add("Accept", "text/html")

		resp, err := client.Do(req)
		if err != nil{
			fmt.Println("Error in get = ", err)
		}

		doc, err := html.Parse(resp.Body)
		if err != nil{
			fmt.Println("Error in parsing response in first task = ", err)
			resp.Body.Close()
			continue
		}

		getFilms(doc, out)

		resp.Body.Close()
	}
	close(out)
}

func second(in, out chan interface{}){
	for node := range in{
		//fmt.Println("receive second")
		film := &movie{}
		fillMovie(node.(*html.Node), film, out, time.Now())
		fmt.Println(film)
	}
	close(out)
}

func third(in, out chan interface{}){
	for data := range in{
		//fmt.Println("receive third")
		ch := data.(chunk)
		t := time.Now()
		findInfo(ch.node, ch.film)
		fmt.Println("since third = ", time.Since(t))
		out <- ch.film
	}
	close(out)
}

func avito(){
	links := []string{
		"https://www.ivi.ru/movies/2017",
		//"https://www.kinopoisk.ru/lists/top500/",
	}
	workers := []ws{first, second, third}
	in := make(chan interface{}, 50)

	go func(in chan interface{}) {
		for _, link := range links{
			in <- link
		}
		close(in)
	}(in)

	for _, worker := range workers{
		tmp := func()chan interface{}{
			out := make(chan interface{}, 50)
			go worker(in, out)
			return out
		}()

		in = tmp
	}

	for data := range in{
		fmt.Println("data = ", data.(*movie))
	}

}
func main() {
	//genereator()
	avito()
}

