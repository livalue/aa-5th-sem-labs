package main

// #include <time.h>
import "C"

import (
	"bufio"
	"crypto/md5"
	"encoding/hex"
	"flag"
	"fmt"
	"github.com/jedib0t/go-pretty/text"
	"io"
	"log"
	"os"
	"strings"
	//"time"

	"github.com/jedib0t/go-pretty/table"
)

var path *string

func wait(out chan struct{}){
	<-out
}

func create_file_name(file_name string) string{
	return fmt.Sprintf("%s%c%s.txt", *path, os.PathSeparator, file_name)
}


type (
	tuple struct{
		start my_time
		end my_time
	}
	
	task struct {
		target     interface{}
		firstTime  tuple
		secondTime tuple
		thirdTime  tuple
	}

	ws func(in, out chan task)

	my_time int64
)

func digit_in(str string) (res bool){
	var digits = "0123456789"
label:
	for _, digit := range digits{
		for _, char := range str{
			if char == digit{
				res = true
				break label
			}
		}
	}
	return
}

func measureTime() my_time{
	return my_time(C.clock())
}

func newTuple(start my_time)*tuple{

	return &tuple{
		start: start,
		end: measureTime(),
	}
}

func first(in, out chan task){
	for t := range in{
		start := measureTime()
		lines := []string{}
		target := t.target.(*os.File)
		buffer := bufio.NewReader(target)
		for {
			str, err := buffer.ReadString('\n')
			if err != nil{
				if err == io.EOF{
					break
				}else{
					target.Close()
					log.Fatal("uknown error in reading file = ", err)
				}
			}

			if len(str) <= 20{
				lines = append(lines, str)
			}
		}
		target.Close()
		t.target = lines
		t.firstTime = *newTuple(start)
		out <- t
	}
	close(out)
}

func second(in, out chan task){
	for t := range in{
		start := measureTime()
		lines := t.target.([]string)
		for i, line := range lines{
			if digit_in(strings.TrimRight(line, "\n")){
				lines[i], lines[len(lines) - 1] = lines[len(lines) - 1], lines[i]
				lines = lines[:len(lines) - 1]
			}
		}
		t.target = lines
		t.secondTime = *newTuple(start)
		out <- t
	}
	close(out)
}

func third(in, out chan task){
	for t := range in{
		start := measureTime()
		lines := t.target.([]string)
		for i, line := range lines{
			res := md5.Sum([]byte(line))
			lines[i] = hex.EncodeToString(res[:])
		}
		t.target = lines
		t.thirdTime = *newTuple(start)
		out <- t
	}
	close(out)
}

func avg_print(counter int, times ...my_time){
	if counter == 0{
		return
	}
	for i, elem := range times{
		fmt.Printf("Среднее время, проведенное в %d линии = %.3f\n", i + 1, float64(elem) / float64(counter))
	}
}

func get_results(in chan task, out chan struct{}){

	counter := 0

	var time1, time2, time3, waiting my_time


	tab := table.NewWriter()
	tab.SetColumnConfigs([]table.ColumnConfig{
		{Number: 2, Align: text.AlignCenter},
		{Number: 3, Align: text.AlignCenter},
		{Number: 4, Align: text.AlignCenter},
	})
	tab.SetOutputMirror(os.Stdout)
	//goto loop

	tab.AppendHeader(table.Row{
		"#",
		"Время1 {start - end}",
		"Время2 {start - end}",
		"Время3 {start - end}",
	})
//loop:
	for t := range in{
		//continue
		tab.AppendRow([]interface{}{
			counter + 1,
			fmt.Sprintf("%d - %d", t.firstTime.start, t.firstTime.end ),
			fmt.Sprintf("%d - %d", t.secondTime.start, t.secondTime.end ),
			fmt.Sprintf("%d - %d", t.thirdTime.start, t.thirdTime.end ),
		})
		tab.AppendSeparator()
		counter++
		time1 += t.firstTime.end - t.firstTime.start
		time2 += t.secondTime.end - t.secondTime.start
		time3 += t.thirdTime.end - t.thirdTime.start
		waiting += t.secondTime.start - t.firstTime.end + t.thirdTime.start - t.secondTime.end
	}
	//goto exit_
	tab.Render()
	fmt.Println()
	fmt.Println("Суммарное время, проведенное задачами на 1 линии = ", time1)
	fmt.Println("Суммарное время, проведенное задачами на 2 линии = ", time2)
	fmt.Println("Суммарное время, проведенное задачами на 3 линии = ", time3)
	fmt.Println("Время ожидания между переходом задачи из конвейера в конвейер = ", waiting)

	avg_print(counter, time1, time2, time3)
//exit_:
	out <- struct{}{}
}

func main(){
	//start := int64(C.clock())
	path = flag.String("path", "./next", "path to files")
	amount := flag.Int("amount", 0, "amount of files")
	flag.Parse()

	workers := []ws{first, second, third}
	in := make(chan task, *amount)
	tmp := in
	out := make(chan struct{})


	for _, worker :=  range workers{
		in = func() chan task{
			out := make(chan task, *amount)
			go worker(in, out)
			return out
		}()
	}

	go get_results(in, out)

	for i := 0; i < *amount; i++{
		file_name := create_file_name(fmt.Sprintf("%d", i + 1))
		f, err := os.Open(file_name)
		if err != nil{
			fmt.Println("error in openning file", file_name, "due to", err)
			break
		}
		tmp <- task{target: f}
	}

	close(tmp)
	wait(out)

	//fmt.Println(int64(C.clock()) - start)
}
