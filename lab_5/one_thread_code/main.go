package main
// #include <time.h>
import "C"

import (
"bufio"
//"bytes"
"crypto/md5"
"encoding/hex"
"flag"
"fmt"
//"github.com/jedib0t/go-pretty/text"
"io"
"log"
"os"
"strings"
//"time"

//"github.com/jedib0t/go-pretty/table"
)

var path *string


func create_file_name(file_name string) string{
	return fmt.Sprintf("%s%c%s.txt", *path, os.PathSeparator, file_name)
}


type (
	tuple struct{
		start my_time
		end my_time
	}

	task struct {
		target     interface{}
		firstTime  tuple
		secondTime tuple
		thirdTime  tuple
	}

	ws func(in, out chan task)

	my_time int64
)

func digit_in(str string) (res bool){
	var digits = "0123456789"
label:
	for _, digit := range digits{
		for _, char := range str{
			if char == digit{
				res = true
				break label
			}
		}
	}
	return
}



func first(file string)[]string{
	lines := []string{}
	f, err := os.Open(file)
	if err != nil{
		log.Fatal("error in openning files")
	}
	defer f.Close()
	buffer := bufio.NewReader(f)
	for {
		str, err := buffer.ReadString('\n')
		if err != nil{
			if err == io.EOF{
				break
			}else{
				log.Fatal("uknown error in reading file = ", err)
			}
		}

		if len(str) <= 20{
			lines = append(lines, str)
		}
	}
	return lines
}

func second(lines []string)[]string{
	for i, line := range lines{
		if digit_in(strings.TrimRight(line, "\n")){
			lines[i], lines[len(lines) - 1] = lines[len(lines) - 1], lines[i]
			lines = lines[:len(lines) - 1]
		}
	}
	return lines
}

func third(lines []string){
	for i, line := range lines{
		res := md5.Sum([]byte(line))
		lines[i] = hex.EncodeToString(res[:])
	}
}

func main(){
	start := int64(C.clock())
	path = flag.String("path", "./next", "path to files")
	amount := flag.Int("amount", 0, "amount of files")
	flag.Parse()

	for i:= 0; i < *amount; i++{
		lines := first(create_file_name(fmt.Sprintf("%d", i + 1)))
		lines = second(lines)
		third(lines)
	}

	fmt.Println(int64(C.clock()) - start)
}
