func first(in, out chan task){
	for t := range in{
		start := measureTime()
		lines := []string{}
		target := t.target.(*os.File)
		buffer := bufio.NewReader(target)
		for {
			str, err := buffer.ReadString('\n')
			if err != nil{
				if err == io.EOF{
					break
				}else{
					target.Close()
					log.Fatal("uknown error in reading file = ", err)
				}
			}

			if len(str) <= 20{
				lines = append(lines, str)
			}
		}
		target.Close()
		t.target = lines
		t.firstTime = *newTuple(start)
		out <- t
	}
	close(out)
}