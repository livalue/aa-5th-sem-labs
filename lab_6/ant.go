package main

import (
	"math"
	"math/rand"
	"time"

	"golang.org/x/tools/container/intsets"
)

const (
	pheromone = 1.0
)

func init(){
	rand.Seed(time.Now().Unix())
}

func calcData(dist int, q, ph, a, b float64)float64{
	length := q / math.Pow(float64(dist), b)

	phero := math.Pow(ph, a)

	return length * phero
}

func chooseCity(m map[int]float64)int{
	number := rand.Float64()
	currProb := 0.0
	for key, val := range m{
		currProb += val
		if number < currProb{
			return key
		}
	}
	return 0
}

func (m Matrix) Ant(a, b, p , q float64, maxTime int)*Route{
	pheromones := NewPheromone(len(m))
	r := &Route{length: intsets.MaxInt}

	for i := 1; i < maxTime; i++{

		for k := 0; k < len(m); k++{
			ant := NewAnt(len(m))

			for !(len(ant.cities) == 1 && ant.cities[0] == 1){
				if len(ant.cities) == 1{
					ant.append(1)
				}
				tmp := make(map[int]float64)
				from := ant.cities[0]
				var znam float64

				for _, elem := range ant.cities[1:]{
					znam += calcData(
						m.dist(from, elem),
						q,
						pheromones.ph(from, elem),
						a, b,
						)
				}

				for j := 1; j < len(ant.cities); j++{
					to := ant.cities[j]
					numerator := calcData(
						m.dist(from, to),
						q,
						pheromones.ph(from, to),
						a, b,
						)
					tmp[to] = numerator / znam
				}

				newCity := chooseCity(tmp)
				ant.length += m.dist(from, newCity)
				ant.setNextCity(newCity)
				ant.addPath(newCity)
			}

			if ant.length < r.length{
				r.length = ant.length
				r.Route = ant.path
			}
		}
		pheromones.update(p, q, r)
	}
	return r
}
