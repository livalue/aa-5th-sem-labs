package main

import (
	"fmt"
	"math/rand"
	"time"
)

func init(){
	rand.Seed(time.Now().Unix())
}

type Matrix [][]int

type Pheromones [][]float64

type Route struct {
	Route []int
	length int
}

type Ant struct {
	cities []int
	length int
	path []int
}

const amount = 21

func NewMatrix(cities int) Matrix{
	matrix := make([][]int, cities)
	for i, _ := range matrix{
		matrix[i] = make([]int, cities)
	}

	for i := 0; i < cities; i++{
		matrix[i][i] = 1
		for j := i + 1; j < cities; j++{
			tmp := rand.Intn(amount) + 10
			matrix[i][j], matrix[j][i] = tmp, tmp
		}
	}
	return matrix
}

func (m Matrix) print(){
	for _, elem := range m{
		fmt.Println(elem)
	}
}

func NewPheromone(size int) Pheromones{
	pheromones := make([][]float64, size)
	for i, _ := range pheromones{
		pheromones[i] = make([]float64, size)
		for j := 0; j < size; j++{
			if j == i{
				pheromones[i][j] = 0
			}else{
				pheromones[i][j] = pheromone
			}
		}
	}
	return pheromones
}

func NewAnt(size int)*Ant{
	ant := Ant{
		cities: make([]int, size),
		length: 0,
		path: []int{1},
	}
	for i, _ := range ant.cities{
		ant.cities[i] = i + 1
	}

	return &ant
}

func (a *Ant) setNextCity(city int){
	a.cities = a.cities[1:]
	for i, elem := range a.cities{
		if elem == city{
			a.cities[0], a.cities[i] = a.cities[i], a.cities[0]
		}
	}
}

func (p Pheromones) ph(from, to int)float64{
	return  p[from - 1][to - 1]
}

func (p Pheromones) evaporate(ev float64) {
	for _, row := range p{
		for i, _ := range row{
			row[i] *= 1 - ev
		}
	}
}

func (p Pheromones) update(ev, q float64, r *Route){
	p.evaporate(ev)

	for i := 0; i < len(r.Route) - 1; i++{
		from, to := r.Route[i] - 1, r.Route[i + 1] - 1
		p[from][to] += q / float64(r.length)
	}
}

func (a *Ant) append(val int){
	a.cities = append(a.cities, val)
}

func (a *Ant) addPath(val int){
	a.path = append(a.path, val)
}
