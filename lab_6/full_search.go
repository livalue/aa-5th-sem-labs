package main

import (
	"golang.org/x/tools/container/intsets"
)

func swap(arr []int, i, j int){
	arr[i], arr[j] = arr[j], arr[i]
}

func (m Matrix) dist(from, to int) int {
	return m[from - 1][to - 1]
}

func perm(m Matrix, arr []int, index int, r *Route){
	if index >= len(arr) - 2{
		sum := 0
		for i := 0; i < len(arr) - 1; i++{
			sum += m.dist(arr[i], arr[i + 1])
		}
		if sum < r.length{
			tmp := make([]int, len(arr))
			copy(tmp, arr)
			r.length = sum
			r.Route = tmp
		}
		return
	}
	perm(m, arr, index + 1, r)
	for j := index + 1; j < len(arr) - 1; j++{
		swap(arr, index, j)
		perm(m, arr, index + 1, r)
		swap(arr, index, j)
	}

}

func (m Matrix) FullSearch() *Route{
	arr := make([]int, len(m) + 1)
	arr[0], arr[len(m)] = 1, 1
	for i := 1; i < len(m); i++{
		arr[i] = i + 1
	}
	r := &Route{length: intsets.MaxInt}
	perm(m, arr, 1, r)

	return r
}

