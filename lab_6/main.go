package main

import "fmt"
func main(){
	//createCSV()
	//measure()
	var m = NewMatrix(11)
	fmt.Println("Сгенерированная матрица:")
	m.print()
	fmt.Println()
	r1, r2 := m.FullSearch(), m.Ant(0.2, 1, 0.2, 10, 300)

	fmt.Println("Путь, найденный алгоритмом полного перебора:")
	fmt.Println(r1.Route)
	fmt.Println("Длина = ", r1.length)

	fmt.Println()

	fmt.Println("Путь, найденный муравьиным алгоритмом:")
	fmt.Println(r2.Route)
	fmt.Println("Длина = ", r2.length)

}
