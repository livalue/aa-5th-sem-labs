package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"time"
)

const rep = 20
const (
	brute = iota
	ant
)

func getTime(m Matrix, method int) float64 {
	const a, b, p, q, times = 1.0, 0.2, 0.2, 10, 10
	switch method {
	case brute:
		var total int64
		for i := 0; i < rep; i++ {
			start := time.Now()
			_ = m.FullSearch()
			total += time.Since(start).Milliseconds()
		}
		if rep != 0 {
			return float64(total) / float64(rep)
		} else {
			return 0
		}
	case ant:
		var total int64
		for i := 0; i < rep; i++ {
			start := time.Now()
			_ = m.Ant(a, b, p, q, times)
			total += time.Since(start).Milliseconds()
		}
		if rep != 0 {
			return float64(total) / float64(rep)
		} else {
			return 0
		}
	default:
		return 0
	}
}

func measure() {
	f1, _ := os.Create("report/brute_m.txt")
	f2, _ := os.Create("report/ant_m.txt")

	defer func() {
		f1.Close()
		f2.Close()
	}()

	for i := 2; i <= 11; i++ {
		fmt.Println("size = ", i)
		m := NewMatrix(i)

		tb, ta := getTime(m, brute), getTime(m, ant)
		f1.WriteString(fmt.Sprintf("%d %.3f\n", i, tb))
		f2.WriteString(fmt.Sprintf("%d %.3f\n", i, ta))
		fmt.Println(tb, ta)
	}
}

func createCSV() {
	stepA, stepP, stepsT := 0.25, 0.2, []int{50, 100, 200, 300}
	q, b := 10.0, 1.0
	m := NewMatrix(10)
	lb := m.FullSearch().length
	f, _ := os.Create("report/file.csv")
	defer f.Close()
	writer := csv.NewWriter(f)
	defer writer.Flush()
	writer.Write([]string{"a", "p", "gen", "diff"})
	for a := stepA; a < 1 + stepA / 2; a += stepA {
		for p := stepP; p < 1+stepP/2; p += stepP {
			for _, t := range stepsT {
				la := m.Ant(a, p, b, q, t).length

				diff := int(100 * (float64(la) / float64(lb) - 1))
				writer.Write(
					[]string{
						fmt.Sprintf("%.2f", a),
						fmt.Sprintf("%.1f", p),
						fmt.Sprintf("%.d", t),
						fmt.Sprintf("%d%%", diff),
					},
				)
			}
		}
	}
}
