package main

import (
	"sort"
)

type tuple struct {
	key, value string
}
type anyDict interface {}

type dict []tuple
type segDict []dict

func (t dict) sort() {
	sort.Slice(t, func(i, j int) bool {
		return t[i].key < t[j].key
	})
}

func (t dict) stohasticSort(){
	obj := readFreq()

	sort.Slice(t, func(i, j int) bool {
		return stohasticGreahter(t[i].key, t[j].key, obj)
	})

}

func (t dict) copy()(res dict){
	for _, tup := range t{
		newTup := tuple{tup.key, tup.value}
		res = append(res, newTup)
	}
	return
}

func (t dict) createSeg() segDict{
	segments := make(segDict, 26)
	for _, tup := range t{
		index := tup.key[0] - 97
		segments[index] = append(segments[index], tup)
	}

	for _, seg := range segments{
		seg.stohasticSort()
	}

	return segments
}

