package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main(){
	//measure()
	fmt.Println("Enter dict like key-value:")
	var dict_ dict
	scanner := bufio.NewScanner(os.Stdin)
	for {
		ok := scanner.Scan()
		if !ok{
			log.Fatal("some")
		}
		str := scanner.Text()
		if str == ""{
			break
		}
		str = strings.Replace(str, " ", "", strings.Count(str, " ") - 1)
		str = strings.ToLower(str)
		arr := strings.Split(str, " ")
		fmt.Println("entered str = ", str)
		dict_ = append(dict_, tuple{arr[0], arr[1]})
	}
	//fmt.Println(dict_)

	binD := dict_.copy()
	binD.sort()

	if val, ok := iterFind("some", dict_); !ok{
		fmt.Println("no such key")
	}else{
		fmt.Println("value = ", val)
	}

	if val, ok := binaryFind("some", binD); !ok{
		fmt.Println("no such key")
	}else{
		fmt.Println("value = ", val)
	}

	if val, ok := stohasticSearch("some", dict_.createSeg()); !ok{
		fmt.Println("no such key")
	}else{
		fmt.Println("value = ", val)
	}
}
