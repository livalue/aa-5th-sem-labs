package main

import "testing"

func TestFirst(t *testing.T) {
	d := dict{
		tuple{"some", "value"},
		tuple{"new", "value"},
		tuple{"key", "value"},
		tuple{"os", "value"},
		tuple{"done", "value"},
	}

	binD := d.copy()
	binD.sort()
	statD := d.createSeg()

	for _, elem := range d{
		res, _ := iterFind(elem.key, d)
		res1, _ := binaryFind(elem.key, binD)
		res2, _ := stohasticSearch(elem.key, statD)

		if !(res1 == res2 && res2 == res){
			t.Error("different anaswers for key = ", elem.key)
		}
	}
}

func TestOne(t *testing.T){
	d := dict{
		tuple{"some", "value"},
	}

	binD := d.copy()
	binD.sort()
	statD := d.createSeg()

	for _, elem := range d{
		res, _ := iterFind(elem.key, d)
		res1, _ := binaryFind(elem.key, binD)
		res2, _ := stohasticSearch(elem.key, statD)

		if !(res1 == res2 && res2 == res){
			t.Error("different anaswers for key = ", elem.key)
		}
	}
}

func TestUnExist(t *testing.T){
	d := dict{
		tuple{"some", "value"},
		tuple{"new", "value"},
		tuple{"key", "value"},
	}

	binD := d.copy()
	binD.sort()
	statD := d.createSeg()

	elem := tuple{"not", "exist"}

	res, _ := iterFind(elem.key, d)
	res1, _ := binaryFind(elem.key, binD)
	res2, _ := stohasticSearch(elem.key, statD)

	if !(res1 == res2 && res2 == res){
		t.Error("different anaswers for key = ", elem.key)
	}
}