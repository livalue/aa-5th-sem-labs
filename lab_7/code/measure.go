package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
)

const (
	times      = 100000
	binAvg = 350
	mFileName  = "ex.txt"
	iter       = "iter"
	bin        = "binary_search"
	stochastic = "statistic method"

	best  = "best"
	avg   = "avg"
	worst = "worst"
)

func measure() {
	file, err := os.Open(mFileName)
	if err != nil {
		log.Fatal("cant open file for mesuring")
	}

	buffer := bufio.NewReader(file)

	var arr []string
	for {
		tmp, err := buffer.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatal("unknown error = ", err)
			}
		}

		arr = append(arr, strings.TrimRight(tmp, "\r\n"))
	}

	d := make(dict, len(arr))

	for i, _ := range d {
		d[i] = tuple{arr[i], arr[rand.Intn(len(arr))]}
	}

	methods := []string{
		bin,
		iter,
		stochastic,
	}

	keys := []string{
		best,
		avg,
		worst,
	}

	for _, key := range keys {
		file, _ := os.Create(key + ".txt")
		for _, name := range methods {
			file.WriteString(fmt.Sprintf("%s %s : %d\n", key, name,
				selectMethod(key, name, d)))
			//time.Sleep(10*time.Second)
		}
	}

}

func selectMethod(caseName, methodName string, d dict) int64 {
	switch methodName {
	case iter:
		return iterMeasure(caseName, d)
	case bin:
		return bindMeasure(caseName, d)
	case stochastic:
		return stochasticMeasure(caseName, d)
	default:
		fmt.Println("unknown")
		return 0
	}
}

func iterMeasure(caseName string, d dict) int64 {
	var keys []string
	switch caseName {
	case best:
		for i := 0; i < times; i++ {
			keys = append(keys, d[0].key)
		}
	case avg:
		for _, elem := range d {
			keys = append(keys, elem.key)
		}
	case worst:
		for i := 0; i < times; i++ {
			keys = append(keys, d[len(d)-1].key)
		}
	default:
		fmt.Println("unknown")
		return 0
	}

	return subMeasure(iter, keys, d)
}

func bindMeasure(caseName string, d dict) int64 {
	var keys []string
	newD := d.copy()
	newD.sort()

	//for i, val := range newD{
	//	if val.key == "lovely"{
	//		log.Fatal("index = ", i, len(d))
	//	}
	//}
	switch caseName {
	case best:
		for i := 0; i < times; i++ {
			keys = append(keys, newD[len(newD)/2].key)
		}
	case avg:
		for i := 0; i < times; i++ {
			keys = append(keys, newD[binAvg].key)
		}
	case worst:
		for i := 0; i < times; i++ {
			keys = append(keys, newD[len(newD)/2 + 1].key)
		}
	default:
		fmt.Println("unknown")
		return 0
	}

	return subMeasure(bin, keys, newD)
}

func stochasticMeasure(caseName string, d dict) int64 {
	var keys []string
	newD := d.createSeg()

	switch caseName {
	case best:
		for i := 0; i < times; i++ {
			keys = append(keys, newD[0][0].key)
		}
	case avg:
		for _, elem := range d {
			keys = append(keys, elem.key)
		}
	case worst:
		index, max := 0, 0
		for i, tup := range newD {
			if len(tup) > max {
				max = len(tup)
				index = i
			}
		}
		tup := newD[index]

		for i := 0; i < times; i++ {
			keys = append(keys, tup[len(tup)-1].key)
		}
	default:
		fmt.Println("unknown")
		return 0
	}

	return subMeasure(stochastic, keys, newD)
}

func subMeasure(method string, keys []string, any anyDict) int64 {

	var amount, count int64
	switch d := any.(type) {
	case dict:
		f := iterFind
		//fmt.Println(keys, method, d[:10])
		if method == bin {
			f = binaryFind
		}

		for _, key := range keys {
			start := time.Now()
			f(key, d)
			tmp := time.Since(start).Nanoseconds()
			amount += tmp
			count += 1
		}
	case segDict:
		f := stohasticSearch

		for _, key := range keys {
			start := time.Now()
			f(key, d)
			tmp := time.Since(start).Nanoseconds()
			amount += tmp
			count += 1
		}

	}

	if count == 0 {
		fmt.Println("some")
		return 0
	} else {
		return amount / count
	}
}
