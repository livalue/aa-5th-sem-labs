package main


func iterFind(key string, dict_ dict)(string, bool){
	for _, tup := range dict_{
		if key == tup.key{
			return tup.value, true
		}
	}
	return "", false
}

func binaryFind(key string, dict_ dict)(string, bool){

	l, r := 0, len(dict_)
	for l < r {
		mid := l + (r - l) / 2
		if dict_[mid].key == key{
			return dict_[mid].value, true
		}

		if dict_[mid].key > key{
			r = mid
		}else{
			l = mid + 1
		}
	}

	return "", false
}

func stohasticSearch(key string, dict_ segDict)(string, bool){
	seg := dict_[key[0] - 97]
	return iterFind(key, seg)
}

