package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

const fileName = "freq.txt"

func readFreq()map[byte]int{
	file, err := os.Open(fileName)
	if err != nil{
		log.Fatal("cant open file")
	}

	obj := make(map[byte]int)

	buffer := bufio.NewReader(file)
	for {
		str, err := buffer.ReadString('\n')
		if err == io.EOF{
			break
		}else if err != nil{
			log.Fatal("unknown error = ", err)
		}

		key, val := byte(0), 0

		_, err = fmt.Sscanf(str, "%c %d", &key, &val)
		if err != nil{
			log.Fatal("err = ", err)
		}

		obj[key] = val
	}


	return obj
}

func stohasticGreahter(str1, str2 string, obj map[byte]int )(res bool){
	minLen := 0
	if minLen = len(str1);minLen > len(str2){
		minLen = len(str2)
		res = true
	}
	for i := 0; i < minLen; i++{
		if str1[i] != str2[i]{
			if obj[str1[i]] > obj[str2[i]]{
				return true
			}else{
				return false
			}
		}
	}
	return
}
