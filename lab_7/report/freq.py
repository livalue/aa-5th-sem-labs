from nltk import word_tokenize
from nltk.probability import FreqDist
import string
import nltk

def remove_spec_chars(text, template):
    return "".join([ch for ch in text if ch not in template])

file_name = "crime.txt"
output = "freq.txt"
text = ""

str = "А а, Б б, В в, Г г, Д д, Е е, Ё ё, Ж ж, З з, И и, Й й, К к, Л л, М м, Н н, О о, П п, Р р, С с, Т т, У у, Ф ф, Х х, Ц ц, Ч ч, Ш ш, Щ щ, Ъ ъ, Ы ы, Ь ь, Э э, Ю ю, Я я"
with open(file_name, "r") as f:
    text = f.read().lower()
    spec_chars = string.punctuation + '\t—“”’‘°' + str.replace(" ", "").replace(",", "")
    text = remove_spec_chars(text, spec_chars)
    text = remove_spec_chars(text, string.digits)
    text = text.replace("\n", " ")
    text = text.split(" ")
    i = 0
    while i < len(text):
        if text[i] == "":
            text.remove(text[i])
            i -= 1
        i += 1
    text = set(text)
    # print(text)
    with open("ex.txt", "w") as out:
        for elem in text:
            if "a" <=elem[0] <= "z":
                out.write(f"{elem}\n")
    # text_tokens = word_tokenize(text)
    # text = nltk.Text(text_tokens)
    # fdist = FreqDist(text)
    #
    # with open(output, "w") as out:
    #     for key, val in list(fdist.items()):
    #         out.write(f"{key} {val}\n")

